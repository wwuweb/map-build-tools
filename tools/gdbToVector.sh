#!/bin/bash

layers="$(ogrinfo -q -ro -so data/geoData/gdb/wwu_campus_map.gdb | perl -pe 's/^[^ ]+ //g and s/ \([^()]+\)$//g')"
for layer in $layers; do
	echo $layer;
	ogr2ogr -f "GeoJSON" -t_srs EPSG:4326 data/geoData/geojson/$layer.json data/geoData/gdb/wwu_campus_map.gdb $layer
done