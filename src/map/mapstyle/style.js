const style = {
    "version": 8,
    "sources": {
        "wwu": {
            "type": "vector",
            "tiles": ["http://localhost:9001/dist/tiles/wwu/{z}/{x}/{y}.pbf"]
        },
        "mapbox": {
            "url": "mapbox://mapbox.mapbox-streets-v7",
            "type": "vector"
        }

    },
    "sprite": "http://localhost:9001/dist/sprite",
    "glyphs": "mapbox://fonts/mapbox/{fontstack}/{range}.pbf",
    "layers": [{
            "id": "background",
            "type": "background",
            "interactive": true,
            "paint": {
                "background-color": "#f8f4f0"
            }
        }, {
            "id": "landuse_overlay_national_park",
            "type": "fill",
            "metadata": {
                "mapbox:group": "1444849388993.3071"
            },
            "source": "mapbox",
            "source-layer": "landuse_overlay",
            "interactive": true,
            "filter": [
                "==",
                "class",
                "national_park"
            ],
            "paint": {
                "fill-color": "#d8e8c8",
                "fill-opacity": 0.75
            }
        }, {
            "id": "landuse_park",
            "type": "fill",
            "metadata": {
                "mapbox:group": "1444849388993.3071"
            },
            "source": "mapbox",
            "source-layer": "landuse",
            "interactive": true,
            "filter": [
                "==",
                "class",
                "park"
            ],
            "paint": {
                "fill-color": "#d8e8c8"
            }
        }, {
            "id": "landuse_cemetery",
            "type": "fill",
            "metadata": {
                "mapbox:group": "1444849388993.3071"
            },
            "source": "mapbox",
            "source-layer": "landuse",
            "interactive": true,
            "filter": [
                "==",
                "class",
                "cemetery"
            ],
            "paint": {
                "fill-color": "#e0e4dd"
            }
        }, {
            "id": "landuse_hospital",
            "type": "fill",
            "metadata": {
                "mapbox:group": "1444849388993.3071"
            },
            "source": "mapbox",
            "source-layer": "landuse",
            "interactive": true,
            "filter": [
                "==",
                "class",
                "hospital"
            ],
            "paint": {
                "fill-color": "#fde"
            }
        }, {
            "id": "landuse_school",
            "type": "fill",
            "metadata": {
                "mapbox:group": "1444849388993.3071"
            },
            "source": "mapbox",
            "source-layer": "landuse",
            "interactive": true,
            "filter": [
                "==",
                "class",
                "school"
            ],
            "paint": {
                "fill-color": "#f0e8f8"
            }
        }, {
            "id": "landuse_wood",
            "type": "fill",
            "metadata": {
                "mapbox:group": "1444849388993.3071"
            },
            "source": "mapbox",
            "source-layer": "landuse",
            "interactive": true,
            "filter": [
                "==",
                "class",
                "wood"
            ],
            "paint": {
                "fill-color": "#6a4",
                "fill-opacity": 0.1
            }
        }, {
            "id": "waterway",
            "type": "line",
            "metadata": {
                "mapbox:group": "1444849382550.77"
            },
            "source": "mapbox",
            "source-layer": "waterway",
            "interactive": true,
            "filter": [
                "all", [
                    "!=",
                    "class",
                    "river"
                ],
                [
                    "!=",
                    "class",
                    "stream"
                ],
                [
                    "!=",
                    "class",
                    "canal"
                ]
            ],
            "layout": {
                "line-cap": "round"
            },
            "paint": {
                "line-color": "#a0c8f0",
                "line-width": {
                    "base": 1.3,
                    "stops": [
                        [
                            13,
                            0.5
                        ],
                        [
                            20,
                            2
                        ]
                    ]
                }
            }
        }, {
            "id": "waterway_river",
            "type": "line",
            "metadata": {
                "mapbox:group": "1444849382550.77"
            },
            "source": "mapbox",
            "source-layer": "waterway",
            "interactive": true,
            "filter": [
                "==",
                "class",
                "river"
            ],
            "layout": {
                "line-cap": "round"
            },
            "paint": {
                "line-color": "#a0c8f0",
                "line-width": {
                    "base": 1.2,
                    "stops": [
                        [
                            11,
                            0.5
                        ],
                        [
                            20,
                            6
                        ]
                    ]
                }
            }
        }, {
            "id": "waterway_stream_canal",
            "type": "line",
            "metadata": {
                "mapbox:group": "1444849382550.77"
            },
            "source": "mapbox",
            "source-layer": "waterway",
            "interactive": true,
            "filter": [
                "in",
                "class",
                "stream",
                "canal"
            ],
            "layout": {
                "line-cap": "round"
            },
            "paint": {
                "line-color": "#a0c8f0",
                "line-width": {
                    "base": 1.3,
                    "stops": [
                        [
                            13,
                            0.5
                        ],
                        [
                            20,
                            6
                        ]
                    ]
                }
            }
        }, {
            "id": "water",
            "type": "fill",
            "metadata": {
                "mapbox:group": "1444849382550.77"
            },
            "source": "mapbox",
            "source-layer": "water",
            "interactive": true,
            "paint": {
                "fill-color": "#a0c8f0"
            }
        }, {
            "id": "water_offset",
            "metadata": {
                "mapbox:group": "1444849382550.77"
            },
            "ref": "water",
            "interactive": true,
            "paint": {
                "fill-color": "white",
                "fill-opacity": 0.3,
                "fill-translate": [
                    0,
                    2.5
                ]
            }
        }, {
            "id": "aeroway_fill",
            "type": "fill",
            "metadata": {
                "mapbox:group": "1444849371739.5945"
            },
            "source": "mapbox",
            "source-layer": "aeroway",
            "minzoom": 11,
            "interactive": true,
            "filter": [
                "==",
                "$type",
                "Polygon"
            ],
            "paint": {
                "fill-color": "#f0ede9",
                "fill-opacity": 0.7
            }
        }, {
            "id": "aeroway_runway",
            "type": "line",
            "metadata": {
                "mapbox:group": "1444849371739.5945"
            },
            "source": "mapbox",
            "source-layer": "aeroway",
            "minzoom": 11,
            "interactive": true,
            "filter": [
                "all", [
                    "==",
                    "$type",
                    "LineString"
                ],
                [
                    "==",
                    "type",
                    "runway"
                ]
            ],
            "paint": {
                "line-color": "#f0ede9",
                "line-width": {
                    "base": 1.2,
                    "stops": [
                        [
                            11,
                            3
                        ],
                        [
                            20,
                            16
                        ]
                    ]
                }
            }
        }, {
            "id": "aeroway_taxiway",
            "type": "line",
            "metadata": {
                "mapbox:group": "1444849371739.5945"
            },
            "source": "mapbox",
            "source-layer": "aeroway",
            "minzoom": 11,
            "interactive": true,
            "filter": [
                "all", [
                    "==",
                    "$type",
                    "LineString"
                ],
                [
                    "==",
                    "type",
                    "taxiway"
                ]
            ],
            "paint": {
                "line-color": "#f0ede9",
                "line-width": {
                    "base": 1.2,
                    "stops": [
                        [
                            11,
                            0.5
                        ],
                        [
                            20,
                            6
                        ]
                    ]
                }
            }
        }, {
            "id": "building",
            "type": "fill",
            "metadata": {
                "mapbox:group": "1444849364238.8171"
            },
            "source": "mapbox",
            "source-layer": "building",
            "interactive": true,
            "paint": {
                "fill-color": {
                    "base": 1,
                    "stops": [
                        [
                            15.5,
                            "#f2eae2"
                        ],
                        [
                            16,
                            "#dfdbd7"
                        ]
                    ]
                }
            }
        }, {
            "id": "building_top",
            "metadata": {
                "mapbox:group": "1444849364238.8171"
            },
            "ref": "building",
            "interactive": true,
            "paint": {
                "fill-color": "#f2eae2",
                "fill-opacity": {
                    "base": 1,
                    "stops": [
                        [
                            15,
                            0
                        ],
                        [
                            16,
                            1
                        ]
                    ]
                },
                "fill-translate": {
                    "stops": [
                        [
                            15, [
                                0,
                                0
                            ]
                        ],
                        [
                            16, [-2, -2]
                        ]
                    ],
                    "base": 1
                },
                "fill-outline-color": "#dfdbd7"
            }
        }, {
            "id": "tunnel_motorway_link_casing",
            "type": "line",
            "metadata": {
                "mapbox:group": "1444849354174.1904"
            },
            "source": "mapbox",
            "source-layer": "road",
            "interactive": true,
            "filter": [
                "all", [
                    "==",
                    "structure",
                    "tunnel"
                ],
                [
                    "==",
                    "class",
                    "motorway_link"
                ]
            ],
            "layout": {
                "line-join": "round",
                "visibility": "visible"
            },
            "paint": {
                "line-color": "#e9ac77",
                "line-dasharray": [
                    0.5,
                    0.25
                ],
                "line-width": {
                    "base": 1.2,
                    "stops": [
                        [
                            12,
                            1
                        ],
                        [
                            13,
                            3
                        ],
                        [
                            14,
                            4
                        ],
                        [
                            20,
                            15
                        ]
                    ]
                },
                "line-opacity": 1
            }
        }, {
            "id": "tunnel_service_track_casing",
            "type": "line",
            "metadata": {
                "mapbox:group": "1444849354174.1904"
            },
            "source": "mapbox",
            "source-layer": "road",
            "interactive": true,
            "filter": [
                "all", [
                    "==",
                    "structure",
                    "tunnel"
                ],
                [
                    "in",
                    "class",
                    "service",
                    "track"
                ]
            ],
            "layout": {
                "line-join": "round"
            },
            "paint": {
                "line-color": "#cfcdca",
                "line-dasharray": [
                    0.5,
                    0.25
                ],
                "line-width": {
                    "base": 1.2,
                    "stops": [
                        [
                            15,
                            1
                        ],
                        [
                            16,
                            4
                        ],
                        [
                            20,
                            11
                        ]
                    ]
                }
            }
        }, {
            "id": "tunnel_link_casing",
            "type": "line",
            "metadata": {
                "mapbox:group": "1444849354174.1904"
            },
            "source": "mapbox",
            "source-layer": "road",
            "interactive": true,
            "filter": [
                "all", [
                    "==",
                    "structure",
                    "tunnel"
                ],
                [
                    "==",
                    "class",
                    "link"
                ]
            ],
            "layout": {
                "line-join": "round"
            },
            "paint": {
                "line-color": "#e9ac77",
                "line-width": {
                    "base": 1.2,
                    "stops": [
                        [
                            12,
                            1
                        ],
                        [
                            13,
                            3
                        ],
                        [
                            14,
                            4
                        ],
                        [
                            20,
                            15
                        ]
                    ]
                },
                "line-opacity": 1
            }
        }, {
            "id": "tunnel_street_casing",
            "type": "line",
            "metadata": {
                "mapbox:group": "1444849354174.1904"
            },
            "source": "mapbox",
            "source-layer": "road",
            "interactive": true,
            "filter": [
                "all", [
                    "==",
                    "structure",
                    "tunnel"
                ],
                [
                    "in",
                    "class",
                    "street",
                    "street_limited"
                ]
            ],
            "layout": {
                "line-join": "round"
            },
            "paint": {
                "line-color": "#cfcdca",
                "line-width": {
                    "base": 1.2,
                    "stops": [
                        [
                            12,
                            0.5
                        ],
                        [
                            13,
                            1
                        ],
                        [
                            14,
                            4
                        ],
                        [
                            20,
                            15
                        ]
                    ]
                },
                "line-opacity": {
                    "stops": [
                        [
                            12,
                            0
                        ],
                        [
                            12.5,
                            1
                        ]
                    ]
                }
            }
        }, {
            "id": "tunnel_secondary_tertiary_casing",
            "type": "line",
            "metadata": {
                "mapbox:group": "1444849354174.1904"
            },
            "source": "mapbox",
            "source-layer": "road",
            "interactive": true,
            "filter": [
                "all", [
                    "==",
                    "structure",
                    "tunnel"
                ],
                [
                    "in",
                    "class",
                    "secondary",
                    "tertiary"
                ]
            ],
            "layout": {
                "line-join": "round"
            },
            "paint": {
                "line-color": "#e9ac77",
                "line-width": {
                    "base": 1.2,
                    "stops": [
                        [
                            8,
                            1.5
                        ],
                        [
                            20,
                            17
                        ]
                    ]
                },
                "line-opacity": 1
            }
        }, {
            "id": "tunnel_trunk_primary_casing",
            "type": "line",
            "metadata": {
                "mapbox:group": "1444849354174.1904"
            },
            "source": "mapbox",
            "source-layer": "road",
            "interactive": true,
            "filter": [
                "all", [
                    "==",
                    "structure",
                    "tunnel"
                ],
                [
                    "in",
                    "class",
                    "trunk",
                    "primary"
                ]
            ],
            "layout": {
                "line-join": "round"
            },
            "paint": {
                "line-color": "#e9ac77",
                "line-width": {
                    "base": 1.2,
                    "stops": [
                        [
                            5,
                            0.4
                        ],
                        [
                            6,
                            0.6
                        ],
                        [
                            7,
                            1.5
                        ],
                        [
                            20,
                            22
                        ]
                    ]
                }
            }
        }, {
            "id": "tunnel_motorway_casing",
            "type": "line",
            "metadata": {
                "mapbox:group": "1444849354174.1904"
            },
            "source": "mapbox",
            "source-layer": "road",
            "interactive": true,
            "filter": [
                "all", [
                    "==",
                    "structure",
                    "tunnel"
                ],
                [
                    "==",
                    "class",
                    "motorway"
                ]
            ],
            "layout": {
                "line-join": "round",
                "visibility": "visible"
            },
            "paint": {
                "line-color": "#e9ac77",
                "line-dasharray": [
                    0.5,
                    0.25
                ],
                "line-width": {
                    "base": 1.2,
                    "stops": [
                        [
                            5,
                            0.4
                        ],
                        [
                            6,
                            0.6
                        ],
                        [
                            7,
                            1.5
                        ],
                        [
                            20,
                            22
                        ]
                    ]
                }
            }
        }, {
            "id": "tunnel_path_pedestrian",
            "type": "line",
            "metadata": {
                "mapbox:group": "1444849354174.1904"
            },
            "source": "mapbox",
            "source-layer": "road",
            "interactive": true,
            "filter": [
                "all", [
                    "==",
                    "$type",
                    "LineString"
                ],
                [
                    "all", [
                        "==",
                        "structure",
                        "tunnel"
                    ],
                    [
                        "in",
                        "class",
                        "path",
                        "pedestrian"
                    ]
                ]
            ],
            "paint": {
                "line-color": "#cba",
                "line-dasharray": [
                    1.5,
                    0.75
                ],
                "line-width": {
                    "base": 1.2,
                    "stops": [
                        [
                            15,
                            1.2
                        ],
                        [
                            20,
                            4
                        ]
                    ]
                }
            }
        }, {
            "id": "tunnel_motorway_link",
            "metadata": {
                "mapbox:group": "1444849354174.1904"
            },
            "ref": "tunnel_motorway_link_casing",
            "interactive": true,
            "paint": {
                "line-color": "#fc8",
                "line-width": {
                    "base": 1.2,
                    "stops": [
                        [
                            12.5,
                            0
                        ],
                        [
                            13,
                            1.5
                        ],
                        [
                            14,
                            2.5
                        ],
                        [
                            20,
                            11.5
                        ]
                    ]
                }
            }
        }, {
            "id": "tunnel_service_track",
            "metadata": {
                "mapbox:group": "1444849354174.1904"
            },
            "ref": "tunnel_service_track_casing",
            "interactive": true,
            "paint": {
                "line-color": "#fff",
                "line-width": {
                    "base": 1.2,
                    "stops": [
                        [
                            15.5,
                            0
                        ],
                        [
                            16,
                            2
                        ],
                        [
                            20,
                            7.5
                        ]
                    ]
                }
            }
        }, {
            "id": "tunnel_link",
            "metadata": {
                "mapbox:group": "1444849354174.1904"
            },
            "ref": "tunnel_link_casing",
            "interactive": true,
            "paint": {
                "line-color": "#fff4c6",
                "line-width": {
                    "base": 1.2,
                    "stops": [
                        [
                            12.5,
                            0
                        ],
                        [
                            13,
                            1.5
                        ],
                        [
                            14,
                            2.5
                        ],
                        [
                            20,
                            11.5
                        ]
                    ]
                }
            }
        }, {
            "id": "tunnel_street",
            "metadata": {
                "mapbox:group": "1444849354174.1904"
            },
            "ref": "tunnel_street_casing",
            "interactive": true,
            "paint": {
                "line-color": "#fff",
                "line-width": {
                    "base": 1.2,
                    "stops": [
                        [
                            13.5,
                            0
                        ],
                        [
                            14,
                            2.5
                        ],
                        [
                            20,
                            11.5
                        ]
                    ]
                },
                "line-opacity": 1
            }
        }, {
            "id": "tunnel_secondary_tertiary",
            "metadata": {
                "mapbox:group": "1444849354174.1904"
            },
            "ref": "tunnel_secondary_tertiary_casing",
            "interactive": true,
            "paint": {
                "line-color": "#fff4c6",
                "line-width": {
                    "base": 1.2,
                    "stops": [
                        [
                            6.5,
                            0
                        ],
                        [
                            7,
                            0.5
                        ],
                        [
                            20,
                            10
                        ]
                    ]
                }
            }
        }, {
            "id": "tunnel_trunk_primary",
            "metadata": {
                "mapbox:group": "1444849354174.1904"
            },
            "ref": "tunnel_trunk_primary_casing",
            "interactive": true,
            "paint": {
                "line-color": "#fff4c6",
                "line-width": {
                    "base": 1.2,
                    "stops": [
                        [
                            6.5,
                            0
                        ],
                        [
                            7,
                            0.5
                        ],
                        [
                            20,
                            18
                        ]
                    ]
                }
            }
        }, {
            "id": "tunnel_motorway",
            "metadata": {
                "mapbox:group": "1444849354174.1904"
            },
            "ref": "tunnel_motorway_casing",
            "interactive": true,
            "paint": {
                "line-color": "#ffdaa6",
                "line-width": {
                    "base": 1.2,
                    "stops": [
                        [
                            6.5,
                            0
                        ],
                        [
                            7,
                            0.5
                        ],
                        [
                            20,
                            18
                        ]
                    ]
                }
            }
        }, {
            "id": "tunnel_major_rail",
            "type": "line",
            "metadata": {
                "mapbox:group": "1444849354174.1904"
            },
            "source": "mapbox",
            "source-layer": "road",
            "interactive": true,
            "filter": [
                "all", [
                    "==",
                    "structure",
                    "tunnel"
                ],
                [
                    "in",
                    "class",
                    "major_rail",
                    "minor_rail"
                ]
            ],
            "paint": {
                "line-color": "#bbb",
                "line-width": {
                    "base": 1.4,
                    "stops": [
                        [
                            14,
                            0.4
                        ],
                        [
                            15,
                            0.75
                        ],
                        [
                            20,
                            2
                        ]
                    ]
                }
            }
        }, {
            "id": "tunnel_major_rail_hatching",
            "metadata": {
                "mapbox:group": "1444849354174.1904"
            },
            "ref": "tunnel_major_rail",
            "interactive": true,
            "paint": {
                "line-color": "#bbb",
                "line-dasharray": [
                    0.2,
                    8
                ],
                "line-width": {
                    "base": 1.4,
                    "stops": [
                        [
                            14.5,
                            0
                        ],
                        [
                            15,
                            3
                        ],
                        [
                            20,
                            8
                        ]
                    ]
                }
            }
        }, {
            "id": "road_motorway_link_casing",
            "type": "line",
            "metadata": {
                "mapbox:group": "1444849345966.4436"
            },
            "source": "mapbox",
            "source-layer": "road",
            "minzoom": 12,
            "interactive": true,
            "filter": [
                "all", [
                    "==",
                    "class",
                    "motorway_link"
                ],
                [
                    "!in",
                    "structure",
                    "bridge",
                    "tunnel"
                ]
            ],
            "layout": {
                "line-cap": "round",
                "line-join": "round"
            },
            "paint": {
                "line-color": "#e9ac77",
                "line-width": {
                    "base": 1.2,
                    "stops": [
                        [
                            12,
                            1
                        ],
                        [
                            13,
                            3
                        ],
                        [
                            14,
                            4
                        ],
                        [
                            20,
                            15
                        ]
                    ]
                },
                "line-opacity": 1
            }
        }, {
            "id": "road_service_track_casing",
            "type": "line",
            "metadata": {
                "mapbox:group": "1444849345966.4436"
            },
            "source": "mapbox",
            "source-layer": "road",
            "interactive": true,
            "filter": [
                "all", [
                    "in",
                    "class",
                    "service",
                    "track"
                ],
                [
                    "!in",
                    "structure",
                    "bridge",
                    "tunnel"
                ]
            ],
            "layout": {
                "line-cap": "round",
                "line-join": "round"
            },
            "paint": {
                "line-color": "#cfcdca",
                "line-width": {
                    "base": 1.2,
                    "stops": [
                        [
                            15,
                            1
                        ],
                        [
                            16,
                            4
                        ],
                        [
                            20,
                            11
                        ]
                    ]
                }
            }
        }, {
            "id": "road_link_casing",
            "type": "line",
            "metadata": {
                "mapbox:group": "1444849345966.4436"
            },
            "source": "mapbox",
            "source-layer": "road",
            "minzoom": 13,
            "interactive": true,
            "filter": [
                "all", [
                    "==",
                    "class",
                    "link"
                ],
                [
                    "!in",
                    "structure",
                    "bridge",
                    "tunnel"
                ]
            ],
            "layout": {
                "line-cap": "round",
                "line-join": "round",
                "visibility": "visible"
            },
            "paint": {
                "line-color": "#e9ac77",
                "line-width": {
                    "base": 1.2,
                    "stops": [
                        [
                            12,
                            1
                        ],
                        [
                            13,
                            3
                        ],
                        [
                            14,
                            4
                        ],
                        [
                            20,
                            15
                        ]
                    ]
                },
                "line-opacity": 1
            }
        }, {
            "id": "road_street_casing",
            "type": "line",
            "metadata": {
                "mapbox:group": "1444849345966.4436"
            },
            "source": "mapbox",
            "source-layer": "road",
            "interactive": true,
            "filter": [
                "all", [
                    "==",
                    "$type",
                    "LineString"
                ],
                [
                    "all", [
                        "in",
                        "class",
                        "street",
                        "street_limited"
                    ],
                    [
                        "!in",
                        "structure",
                        "bridge",
                        "tunnel"
                    ]
                ]
            ],
            "layout": {
                "line-cap": "round",
                "line-join": "round"
            },
            "paint": {
                "line-color": "#cfcdca",
                "line-width": {
                    "base": 1.2,
                    "stops": [
                        [
                            12,
                            0.5
                        ],
                        [
                            13,
                            1
                        ],
                        [
                            14,
                            4
                        ],
                        [
                            20,
                            15
                        ]
                    ]
                },
                "line-opacity": {
                    "stops": [
                        [
                            12,
                            0
                        ],
                        [
                            12.5,
                            1
                        ]
                    ]
                }
            }
        }, {
            "id": "road_secondary_tertiary_casing",
            "type": "line",
            "metadata": {
                "mapbox:group": "1444849345966.4436"
            },
            "source": "mapbox",
            "source-layer": "road",
            "interactive": true,
            "filter": [
                "all", [
                    "in",
                    "class",
                    "secondary",
                    "tertiary"
                ],
                [
                    "!in",
                    "structure",
                    "bridge",
                    "tunnel"
                ]
            ],
            "layout": {
                "line-cap": "round",
                "line-join": "round",
                "visibility": "visible"
            },
            "paint": {
                "line-color": "#e9ac77",
                "line-width": {
                    "base": 1.2,
                    "stops": [
                        [
                            8,
                            1.5
                        ],
                        [
                            20,
                            17
                        ]
                    ]
                },
                "line-opacity": 1
            }
        }, {
            "id": "road_trunk_primary_casing",
            "type": "line",
            "metadata": {
                "mapbox:group": "1444849345966.4436"
            },
            "source": "mapbox",
            "source-layer": "road",
            "interactive": true,
            "filter": [
                "all", [
                    "in",
                    "class",
                    "trunk",
                    "primary"
                ],
                [
                    "!in",
                    "structure",
                    "bridge",
                    "tunnel"
                ]
            ],
            "layout": {
                "line-cap": "round",
                "line-join": "round",
                "visibility": "visible"
            },
            "paint": {
                "line-color": "#e9ac77",
                "line-width": {
                    "base": 1.2,
                    "stops": [
                        [
                            5,
                            0.4
                        ],
                        [
                            6,
                            0.6
                        ],
                        [
                            7,
                            1.5
                        ],
                        [
                            20,
                            22
                        ]
                    ]
                },
                "line-opacity": 1
            }
        }, {
            "id": "road_motorway_casing",
            "type": "line",
            "metadata": {
                "mapbox:group": "1444849345966.4436"
            },
            "source": "mapbox",
            "source-layer": "road",
            "minzoom": 5,
            "interactive": true,
            "filter": [
                "all", [
                    "==",
                    "class",
                    "motorway"
                ],
                [
                    "!in",
                    "structure",
                    "bridge",
                    "tunnel"
                ]
            ],
            "layout": {
                "line-cap": "round",
                "line-join": "round",
                "visibility": "visible"
            },
            "paint": {
                "line-color": "#e9ac77",
                "line-width": {
                    "base": 1.2,
                    "stops": [
                        [
                            5,
                            0.4
                        ],
                        [
                            6,
                            0.6
                        ],
                        [
                            7,
                            1.5
                        ],
                        [
                            20,
                            22
                        ]
                    ]
                }
            }
        }, {
            "id": "road_path_pedestrian",
            "type": "line",
            "metadata": {
                "mapbox:group": "1444849345966.4436"
            },
            "source": "mapbox",
            "source-layer": "road",
            "interactive": true,
            "filter": [
                "all", [
                    "==",
                    "$type",
                    "LineString"
                ],
                [
                    "all", [
                        "in",
                        "class",
                        "path",
                        "pedestrian"
                    ],
                    [
                        "!in",
                        "structure",
                        "bridge",
                        "tunnel"
                    ]
                ]
            ],
            "paint": {
                "line-color": "#cba",
                "line-dasharray": [
                    1.5,
                    0.75
                ],
                "line-width": {
                    "base": 1.2,
                    "stops": [
                        [
                            15,
                            1.2
                        ],
                        [
                            20,
                            4
                        ]
                    ]
                }
            }
        }, {
            "id": "road_motorway_link",
            "metadata": {
                "mapbox:group": "1444849345966.4436"
            },
            "ref": "road_motorway_link_casing",
            "interactive": true,
            "paint": {
                "line-color": "#fc8",
                "line-width": {
                    "base": 1.2,
                    "stops": [
                        [
                            12.5,
                            0
                        ],
                        [
                            13,
                            1.5
                        ],
                        [
                            14,
                            2.5
                        ],
                        [
                            20,
                            11.5
                        ]
                    ]
                }
            }
        }, {
            "id": "road_service_track",
            "metadata": {
                "mapbox:group": "1444849345966.4436"
            },
            "ref": "road_service_track_casing",
            "interactive": true,
            "paint": {
                "line-color": "#fff",
                "line-width": {
                    "base": 1.2,
                    "stops": [
                        [
                            15.5,
                            0
                        ],
                        [
                            16,
                            2
                        ],
                        [
                            20,
                            7.5
                        ]
                    ]
                }
            }
        }, {
            "id": "road_link",
            "metadata": {
                "mapbox:group": "1444849345966.4436"
            },
            "ref": "road_link_casing",
            "interactive": true,
            "paint": {
                "line-color": "#fea",
                "line-width": {
                    "base": 1,
                    "stops": [
                        [
                            12.5,
                            0
                        ],
                        [
                            13,
                            1.5
                        ],
                        [16, 10.5],
                        [18, 37],
                        [20, 143]
                    ]
                }
            }
        }, {
            "id": "road_street",
            "metadata": {
                "mapbox:group": "1444849345966.4436"
            },
            "ref": "road_street_casing",
            "interactive": true,
            "paint": {
                "line-color": "rgba(178,178,178,0.6)",
                "line-width": {
                    "base": 1,
                    "stops": [
                        [13.5, 0],
                        [16, 10.5],
                        [18, 37],
                        [20, 143]
                    ]
                },
                "line-opacity": 1
            }
        }, {
            "id": "road_secondary_tertiary",
            "metadata": {
                "mapbox:group": "1444849345966.4436"
            },
            "ref": "road_secondary_tertiary_casing",
            "interactive": true,
            "paint": {
                "line-color": "#fea",
                "line-width": {
                    "base": 1.2,
                    "stops": [
                        [
                            6.5,
                            0
                        ],
                        [
                            8,
                            0.5
                        ],
                        [
                            20,
                            13
                        ]
                    ]
                }
            }
        }, {
            "id": "road_trunk_primary",
            "metadata": {
                "mapbox:group": "1444849345966.4436"
            },
            "ref": "road_trunk_primary_casing",
            "interactive": true,
            "paint": {
                "line-color": "#fea",
                "line-width": {
                    "base": 1.2,
                    "stops": [
                        [
                            6.5,
                            0
                        ],
                        [
                            7,
                            0.5
                        ],
                        [
                            20,
                            18
                        ]
                    ]
                }
            }
        }, {
            "id": "road_motorway",
            "metadata": {
                "mapbox:group": "1444849345966.4436"
            },
            "ref": "road_motorway_casing",
            "interactive": true,
            "paint": {
                "line-color": "#fc8",
                "line-width": {
                    "base": 1.2,
                    "stops": [
                        [
                            6.5,
                            0
                        ],
                        [
                            7,
                            0.5
                        ],
                        [
                            20,
                            18
                        ]
                    ]
                }
            }
        }, {
            "id": "road_major_rail",
            "type": "line",
            "metadata": {
                "mapbox:group": "1444849345966.4436"
            },
            "source": "mapbox",
            "source-layer": "road",
            "interactive": true,
            "filter": [
                "all", [
                    "==",
                    "class",
                    "major_rail"
                ],
                [
                    "!in",
                    "structure",
                    "bridge",
                    "tunnel"
                ]
            ],
            "paint": {
                "line-color": "#bbb",
                "line-width": {
                    "base": 1.4,
                    "stops": [
                        [
                            14,
                            0.4
                        ],
                        [
                            15,
                            0.75
                        ],
                        [
                            20,
                            2
                        ]
                    ]
                }
            }
        }, {
            "id": "road_major_rail_hatching",
            "metadata": {
                "mapbox:group": "1444849345966.4436"
            },
            "ref": "road_major_rail",
            "interactive": true,
            "paint": {
                "line-color": "#bbb",
                "line-dasharray": [
                    0.2,
                    8
                ],
                "line-width": {
                    "base": 1.4,
                    "stops": [
                        [
                            14.5,
                            0
                        ],
                        [
                            15,
                            3
                        ],
                        [
                            20,
                            8
                        ]
                    ]
                }
            }
        }, {
            "id": "bridge_motorway_link_casing",
            "type": "line",
            "metadata": {
                "mapbox:group": "1444849334699.1902"
            },
            "source": "mapbox",
            "source-layer": "road",
            "interactive": true,
            "filter": [
                "all", [
                    "==",
                    "structure",
                    "bridge"
                ],
                [
                    "==",
                    "class",
                    "motorway_link"
                ]
            ],
            "layout": {
                "line-join": "round"
            },
            "paint": {
                "line-color": "#e9ac77",
                "line-width": {
                    "base": 1.2,
                    "stops": [
                        [
                            12,
                            1
                        ],
                        [
                            13,
                            3
                        ],
                        [
                            14,
                            4
                        ],
                        [
                            20,
                            15
                        ]
                    ]
                },
                "line-opacity": 1
            }
        }, {
            "id": "bridge_service_track_casing",
            "type": "line",
            "metadata": {
                "mapbox:group": "1444849334699.1902"
            },
            "source": "mapbox",
            "source-layer": "road",
            "interactive": true,
            "filter": [
                "all", [
                    "==",
                    "structure",
                    "bridge"
                ],
                [
                    "in",
                    "class",
                    "service",
                    "track"
                ]
            ],
            "layout": {
                "line-join": "round"
            },
            "paint": {
                "line-color": "#cfcdca",
                "line-width": {
                    "base": 1.2,
                    "stops": [
                        [
                            15,
                            1
                        ],
                        [
                            16,
                            4
                        ],
                        [
                            20,
                            11
                        ]
                    ]
                }
            }
        }, {
            "id": "bridge_link_casing",
            "type": "line",
            "metadata": {
                "mapbox:group": "1444849334699.1902"
            },
            "source": "mapbox",
            "source-layer": "road",
            "interactive": true,
            "filter": [
                "all", [
                    "==",
                    "structure",
                    "bridge"
                ],
                [
                    "==",
                    "class",
                    "link"
                ]
            ],
            "layout": {
                "line-join": "round"
            },
            "paint": {
                "line-color": "#e9ac77",
                "line-width": {
                    "base": 1.2,
                    "stops": [
                        [
                            12,
                            1
                        ],
                        [
                            13,
                            3
                        ],
                        [
                            14,
                            4
                        ],
                        [
                            20,
                            15
                        ]
                    ]
                },
                "line-opacity": 1
            }
        }, {
            "id": "bridge_street_casing",
            "type": "line",
            "metadata": {
                "mapbox:group": "1444849334699.1902"
            },
            "source": "mapbox",
            "source-layer": "road",
            "interactive": true,
            "filter": [
                "all", [
                    "==",
                    "structure",
                    "bridge"
                ],
                [
                    "in",
                    "class",
                    "street",
                    "street_limited"
                ]
            ],
            "layout": {
                "line-join": "round"
            },
            "paint": {
                "line-color": "#cfcdca",
                "line-width": {
                    "base": 1.2,
                    "stops": [
                        [
                            12,
                            0.5
                        ],
                        [
                            13,
                            1
                        ],
                        [
                            14,
                            4
                        ],
                        [
                            20,
                            15
                        ]
                    ]
                },
                "line-opacity": {
                    "stops": [
                        [
                            12,
                            0
                        ],
                        [
                            12.5,
                            1
                        ]
                    ]
                }
            }
        }, {
            "id": "bridge_secondary_tertiary_casing",
            "type": "line",
            "metadata": {
                "mapbox:group": "1444849334699.1902"
            },
            "source": "mapbox",
            "source-layer": "road",
            "interactive": true,
            "filter": [
                "all", [
                    "==",
                    "structure",
                    "bridge"
                ],
                [
                    "in",
                    "class",
                    "secondary",
                    "tertiary"
                ]
            ],
            "layout": {
                "line-join": "round"
            },
            "paint": {
                "line-color": "#e9ac77",
                "line-width": {
                    "base": 1.2,
                    "stops": [
                        [
                            8,
                            1.5
                        ],
                        [
                            20,
                            17
                        ]
                    ]
                },
                "line-opacity": 1
            }
        }, {
            "id": "bridge_trunk_primary_casing",
            "type": "line",
            "metadata": {
                "mapbox:group": "1444849334699.1902"
            },
            "source": "mapbox",
            "source-layer": "road",
            "interactive": true,
            "filter": [
                "all", [
                    "==",
                    "structure",
                    "bridge"
                ],
                [
                    "in",
                    "class",
                    "trunk",
                    "primary"
                ]
            ],
            "layout": {
                "line-join": "round"
            },
            "paint": {
                "line-color": "#e9ac77",
                "line-width": {
                    "base": 1.2,
                    "stops": [
                        [
                            5,
                            0.4
                        ],
                        [
                            6,
                            0.6
                        ],
                        [
                            7,
                            1.5
                        ],
                        [
                            20,
                            22
                        ]
                    ]
                }
            }
        }, {
            "id": "bridge_motorway_casing",
            "type": "line",
            "metadata": {
                "mapbox:group": "1444849334699.1902"
            },
            "source": "mapbox",
            "source-layer": "road",
            "interactive": true,
            "filter": [
                "all", [
                    "==",
                    "structure",
                    "bridge"
                ],
                [
                    "==",
                    "class",
                    "motorway"
                ]
            ],
            "layout": {
                "line-join": "round"
            },
            "paint": {
                "line-color": "#e9ac77",
                "line-width": {
                    "base": 1.2,
                    "stops": [
                        [
                            5,
                            0.4
                        ],
                        [
                            6,
                            0.6
                        ],
                        [
                            7,
                            1.5
                        ],
                        [
                            20,
                            22
                        ]
                    ]
                }
            }
        }, {
            "id": "bridge_path_pedestrian",
            "type": "line",
            "metadata": {
                "mapbox:group": "1444849334699.1902"
            },
            "source": "mapbox",
            "source-layer": "road",
            "interactive": true,
            "filter": [
                "all", [
                    "==",
                    "$type",
                    "LineString"
                ],
                [
                    "all", [
                        "==",
                        "structure",
                        "bridge"
                    ],
                    [
                        "in",
                        "class",
                        "path",
                        "pedestrian"
                    ]
                ]
            ],
            "paint": {
                "line-color": "#cba",
                "line-dasharray": [
                    1.5,
                    0.75
                ],
                "line-width": {
                    "base": 1.2,
                    "stops": [
                        [
                            15,
                            1.2
                        ],
                        [
                            20,
                            4
                        ]
                    ]
                }
            }
        }, {
            "id": "bridge_motorway_link",
            "metadata": {
                "mapbox:group": "1444849334699.1902"
            },
            "ref": "bridge_motorway_link_casing",
            "interactive": true,
            "paint": {
                "line-color": "#fc8",
                "line-width": {
                    "base": 1.2,
                    "stops": [
                        [
                            12.5,
                            0
                        ],
                        [
                            13,
                            1.5
                        ],
                        [
                            14,
                            2.5
                        ],
                        [
                            20,
                            11.5
                        ]
                    ]
                }
            }
        }, {
            "id": "bridge_service_track",
            "metadata": {
                "mapbox:group": "1444849334699.1902"
            },
            "ref": "bridge_service_track_casing",
            "interactive": true,
            "paint": {
                "line-color": "#fff",
                "line-width": {
                    "base": 1.2,
                    "stops": [
                        [
                            15.5,
                            0
                        ],
                        [
                            16,
                            2
                        ],
                        [
                            20,
                            7.5
                        ]
                    ]
                }
            }
        }, {
            "id": "bridge_link",
            "metadata": {
                "mapbox:group": "1444849334699.1902"
            },
            "ref": "bridge_link_casing",
            "interactive": true,
            "paint": {
                "line-color": "#fea",
                "line-width": {
                    "base": 1.2,
                    "stops": [
                        [
                            12.5,
                            0
                        ],
                        [
                            13,
                            1.5
                        ],
                        [
                            14,
                            2.5
                        ],
                        [
                            20,
                            11.5
                        ]
                    ]
                }
            }
        }, {
            "id": "bridge_street",
            "metadata": {
                "mapbox:group": "1444849334699.1902"
            },
            "ref": "bridge_street_casing",
            "interactive": true,
            "paint": {
                "line-color": "#fff",
                "line-width": {
                    "base": 1.2,
                    "stops": [
                        [
                            13.5,
                            0
                        ],
                        [
                            14,
                            2.5
                        ],
                        [
                            20,
                            11.5
                        ]
                    ]
                },
                "line-opacity": 1
            }
        }, {
            "id": "bridge_secondary_tertiary",
            "metadata": {
                "mapbox:group": "1444849334699.1902"
            },
            "ref": "bridge_secondary_tertiary_casing",
            "interactive": true,
            "paint": {
                "line-color": "#fea",
                "line-width": {
                    "base": 1.2,
                    "stops": [
                        [
                            6.5,
                            0
                        ],
                        [
                            7,
                            0.5
                        ],
                        [
                            20,
                            10
                        ]
                    ]
                }
            }
        }, {
            "id": "bridge_trunk_primary",
            "metadata": {
                "mapbox:group": "1444849334699.1902"
            },
            "ref": "bridge_trunk_primary_casing",
            "interactive": true,
            "paint": {
                "line-color": "#fea",
                "line-width": {
                    "base": 1.2,
                    "stops": [
                        [
                            6.5,
                            0
                        ],
                        [
                            7,
                            0.5
                        ],
                        [
                            20,
                            18
                        ]
                    ]
                }
            }
        }, {
            "id": "bridge_motorway",
            "metadata": {
                "mapbox:group": "1444849334699.1902"
            },
            "ref": "bridge_motorway_casing",
            "interactive": true,
            "paint": {
                "line-color": "#fc8",
                "line-width": {
                    "base": 1.2,
                    "stops": [
                        [
                            6.5,
                            0
                        ],
                        [
                            7,
                            0.5
                        ],
                        [
                            20,
                            18
                        ]
                    ]
                }
            }
        }, {
            "id": "bridge_major_rail",
            "type": "line",
            "metadata": {
                "mapbox:group": "1444849334699.1902"
            },
            "source": "mapbox",
            "source-layer": "road",
            "interactive": true,
            "filter": [
                "all", [
                    "==",
                    "structure",
                    "bridge"
                ],
                [
                    "==",
                    "class",
                    "major_rail"
                ]
            ],
            "paint": {
                "line-color": "#bbb",
                "line-width": {
                    "base": 1.4,
                    "stops": [
                        [
                            14,
                            0.4
                        ],
                        [
                            15,
                            0.75
                        ],
                        [
                            20,
                            2
                        ]
                    ]
                }
            }
        }, {
            "id": "bridge_major_rail_hatching",
            "metadata": {
                "mapbox:group": "1444849334699.1902"
            },
            "ref": "bridge_major_rail",
            "interactive": true,
            "paint": {
                "line-color": "#bbb",
                "line-dasharray": [
                    0.2,
                    8
                ],
                "line-width": {
                    "base": 1.4,
                    "stops": [
                        [
                            14.5,
                            0
                        ],
                        [
                            15,
                            3
                        ],
                        [
                            20,
                            8
                        ]
                    ]
                }
            }
        }, {
            "id": "admin_level_3",
            "type": "line",
            "metadata": {
                "mapbox:group": "1444849307123.581"
            },
            "source": "mapbox",
            "source-layer": "admin",
            "interactive": true,
            "filter": [
                "all", [
                    ">=",
                    "admin_level",
                    3
                ],
                [
                    "==",
                    "maritime",
                    0
                ]
            ],
            "layout": {
                "line-join": "round"
            },
            "paint": {
                "line-color": "#9e9cab",
                "line-dasharray": [
                    3,
                    1,
                    1,
                    1
                ],
                "line-width": {
                    "base": 1,
                    "stops": [
                        [
                            4,
                            0.4
                        ],
                        [
                            5,
                            1
                        ],
                        [
                            12,
                            3
                        ]
                    ]
                }
            }
        }, {
            "id": "admin_level_2",
            "type": "line",
            "metadata": {
                "mapbox:group": "1444849307123.581"
            },
            "source": "mapbox",
            "source-layer": "admin",
            "interactive": true,
            "filter": [
                "all", [
                    "==",
                    "admin_level",
                    2
                ],
                [
                    "==",
                    "disputed",
                    0
                ],
                [
                    "==",
                    "maritime",
                    0
                ]
            ],
            "layout": {
                "line-join": "round",
                "line-cap": "round"
            },
            "paint": {
                "line-color": "#9e9cab",
                "line-width": {
                    "base": 1,
                    "stops": [
                        [
                            4,
                            1.4
                        ],
                        [
                            5,
                            2
                        ],
                        [
                            12,
                            8
                        ]
                    ]
                }
            }
        }, {
            "id": "admin_level_2_disputed",
            "type": "line",
            "metadata": {
                "mapbox:group": "1444849307123.581"
            },
            "source": "mapbox",
            "source-layer": "admin",
            "interactive": true,
            "filter": [
                "all", [
                    "==",
                    "admin_level",
                    2
                ],
                [
                    "==",
                    "disputed",
                    1
                ],
                [
                    "==",
                    "maritime",
                    0
                ]
            ],
            "layout": {
                "line-cap": "round"
            },
            "paint": {
                "line-color": "#9e9cab",
                "line-dasharray": [
                    2,
                    2
                ],
                "line-width": {
                    "base": 1,
                    "stops": [
                        [
                            4,
                            1.4
                        ],
                        [
                            5,
                            2
                        ],
                        [
                            12,
                            8
                        ]
                    ]
                }
            }
        }, {
            "id": "admin_level_3_maritime",
            "type": "line",
            "metadata": {
                "mapbox:group": "1444849307123.581"
            },
            "source": "mapbox",
            "source-layer": "admin",
            "interactive": true,
            "filter": [
                "all", [
                    ">=",
                    "admin_level",
                    3
                ],
                [
                    "==",
                    "maritime",
                    1
                ]
            ],
            "layout": {
                "line-join": "round"
            },
            "paint": {
                "line-color": "#a0c8f0",
                "line-opacity": 0.5,
                "line-dasharray": [
                    3,
                    1,
                    1,
                    1
                ],
                "line-width": {
                    "base": 1,
                    "stops": [
                        [
                            4,
                            0.4
                        ],
                        [
                            5,
                            1
                        ],
                        [
                            12,
                            3
                        ]
                    ]
                }
            }
        }, {
            "id": "admin_level_2_maritime",
            "type": "line",
            "metadata": {
                "mapbox:group": "1444849307123.581"
            },
            "source": "mapbox",
            "source-layer": "admin",
            "interactive": true,
            "filter": [
                "all", [
                    "==",
                    "admin_level",
                    2
                ],
                [
                    "==",
                    "maritime",
                    1
                ]
            ],
            "layout": {
                "line-cap": "round"
            },
            "paint": {
                "line-color": "#a0c8f0",
                "line-opacity": 0.5,
                "line-width": {
                    "base": 1,
                    "stops": [
                        [
                            4,
                            1.4
                        ],
                        [
                            5,
                            2
                        ],
                        [
                            12,
                            8
                        ]
                    ]
                }
            }
        }, {
            "id": "water_label",
            "type": "symbol",
            "metadata": {
                "mapbox:group": "1444849320558.5054"
            },
            "source": "mapbox",
            "source-layer": "water_label",
            "interactive": true,
            "filter": [
                "==",
                "$type",
                "Point"
            ],
            "layout": {
                "text-font": [
                    "Open Sans Italic",
                    "Arial Unicode MS Regular"
                ],
                "text-field": "{name_en}",
                "text-max-width": 5,
                "text-size": 12
            },
            "paint": {
                "text-color": "#74aee9",
                "text-halo-width": 1.5,
                "text-halo-color": "rgba(255,255,255,0.7)"
            }
        },



        {
            "id": "rail_station_label",
            "type": "symbol",
            "metadata": {
                "mapbox:group": "1444849297111.495"
            },
            "source": "mapbox",
            "source-layer": "rail_station_label",
            "interactive": true,
            "layout": {
                "text-size": 12,
                "icon-image": "{maki}-11",
                "text-font": [
                    "Open Sans Semibold",
                    "Arial Unicode MS Bold"
                ],
                "text-padding": 2,
                "visibility": "visible",
                "text-offset": [
                    0,
                    0.6
                ],
                "text-anchor": "top",
                "text-field": "{name_en}",
                "text-max-width": 9
            },
            "paint": {
                "text-color": "#666",
                "text-halo-color": "#ffffff",
                "text-halo-width": 1,
                "text-halo-blur": 0.5
            }
        },

        {
            "id": "airport_label",
            "type": "symbol",
            "metadata": {
                "mapbox:group": "1444849297111.495"
            },
            "source": "mapbox",
            "source-layer": "airport_label",
            "minzoom": 11,
            "interactive": true,
            "filter": [
                "all", [
                    "==",
                    "$type",
                    "Point"
                ],
                [
                    "in",
                    "scalerank",
                    1,
                    2,
                    3
                ]
            ],
            "layout": {
                "icon-image": "{maki}-11",
                "text-font": [
                    "Open Sans Semibold",
                    "Arial Unicode MS Bold"
                ],
                "text-field": "{name_en}",
                "text-max-width": 9,
                "text-padding": 2,
                "text-offset": [
                    0,
                    0.6
                ],
                "text-anchor": "top",
                "text-size": 12
            },
            "paint": {
                "text-color": "#666",
                "text-halo-color": "#ffffff",
                "text-halo-width": 1,
                "text-halo-blur": 0.5
            }
        },

        {
            "id": "road_label_highway_shield",
            "type": "symbol",
            "metadata": {
                "mapbox:group": "1456163609504.0715"
            },
            "source": "mapbox",
            "source-layer": "road_label",
            "minzoom": 8,
            "interactive": true,
            "filter": [
                "<=",
                "reflen",
                6
            ],
            "layout": {
                "text-field": "{ref}",
                "text-font": [
                    "Open Sans Semibold",
                    "Arial Unicode MS Bold"
                ],
                "text-size": 11,
                "icon-image": "motorway_{reflen}",
                "symbol-placement": {
                    "base": 1,
                    "stops": [
                        [
                            10,
                            "point"
                        ],
                        [
                            11,
                            "line"
                        ]
                    ]
                },
                "symbol-spacing": 500,
                "text-rotation-alignment": "viewport",
                "icon-rotation-alignment": "viewport"
            },
            "paint": {}
        }, {
            "id": "place_label_other",
            "type": "symbol",
            "metadata": {
                "mapbox:group": "1444849272561.29"
            },
            "source": "mapbox",
            "source-layer": "place_label",
            "interactive": true,
            "filter": [
                "in",
                "type",
                "hamlet",
                "suburb",
                "neighbourhood",
                "island",
                "islet"
            ],
            "layout": {
                "text-font": [
                    "Open Sans Bold",
                    "Arial Unicode MS Bold"
                ],
                "text-transform": "uppercase",
                "text-letter-spacing": 0.1,
                "text-field": "{name_en}",
                "text-max-width": 9,
                "text-size": {
                    "base": 1.2,
                    "stops": [
                        [
                            12,
                            10
                        ],
                        [
                            15,
                            14
                        ]
                    ]
                }
            },
            "paint": {
                "text-color": "#633",
                "text-halo-color": "rgba(255,255,255,0.8)",
                "text-halo-width": 1.2
            }
        }, {
            "id": "place_label_village",
            "type": "symbol",
            "metadata": {
                "mapbox:group": "1444849272561.29"
            },
            "source": "mapbox",
            "source-layer": "place_label",
            "interactive": true,
            "filter": [
                "==",
                "type",
                "village"
            ],
            "layout": {
                "text-font": [
                    "Open Sans Regular",
                    "Arial Unicode MS Regular"
                ],
                "text-field": "{name_en}",
                "text-max-width": 8,
                "text-size": {
                    "base": 1.2,
                    "stops": [
                        [
                            10,
                            12
                        ],
                        [
                            15,
                            22
                        ]
                    ]
                }
            },
            "paint": {
                "text-color": "#333",
                "text-halo-color": "rgba(255,255,255,0.8)",
                "text-halo-width": 1.2
            }
        }, {
            "id": "place_label_town",
            "type": "symbol",
            "metadata": {
                "mapbox:group": "1444849272561.29"
            },
            "source": "mapbox",
            "source-layer": "place_label",
            "interactive": true,
            "filter": [
                "==",
                "type",
                "town"
            ],
            "layout": {
                "text-font": [
                    "Open Sans Regular",
                    "Arial Unicode MS Regular"
                ],
                "text-field": "{name_en}",
                "text-max-width": 8,
                "text-size": {
                    "base": 1.2,
                    "stops": [
                        [
                            10,
                            14
                        ],
                        [
                            15,
                            24
                        ]
                    ]
                }
            },
            "paint": {
                "text-color": "#333",
                "text-halo-color": "rgba(255,255,255,0.8)",
                "text-halo-width": 1.2
            }
        }, {
            "id": "place_label_city",
            "type": "symbol",
            "metadata": {
                "mapbox:group": "1444849272561.29"
            },
            "source": "mapbox",
            "source-layer": "place_label",
            "interactive": true,
            "filter": [
                "==",
                "type",
                "city"
            ],
            "layout": {
                "text-font": [
                    "Open Sans Semibold",
                    "Arial Unicode MS Bold"
                ],
                "text-field": "{name_en}",
                "text-max-width": 8,
                "text-size": {
                    "base": 1.2,
                    "stops": [
                        [
                            7,
                            14
                        ],
                        [
                            11,
                            24
                        ]
                    ]
                }
            },
            "paint": {
                "text-color": "#333",
                "text-halo-color": "rgba(255,255,255,0.8)",
                "text-halo-width": 1.2
            }
        }, {
            "id": "marine_label_line_4",
            "type": "symbol",
            "metadata": {
                "mapbox:group": "1444849258897.3083"
            },
            "source": "mapbox",
            "source-layer": "marine_label",
            "interactive": true,
            "filter": [
                "all", [
                    "==",
                    "$type",
                    "LineString"
                ],
                [
                    ">=",
                    "labelrank",
                    4
                ]
            ],
            "layout": {
                "text-font": [
                    "Open Sans Italic",
                    "Arial Unicode MS Regular"
                ],
                "text-field": "{name_en}",
                "text-letter-spacing": 0.2,
                "symbol-placement": "line",
                "text-size": {
                    "stops": [
                        [
                            3,
                            11
                        ],
                        [
                            4,
                            12
                        ]
                    ]
                }
            },
            "paint": {
                "text-color": "#74aee9",
                "text-halo-color": "rgba(255,255,255,0.7)",
                "text-halo-width": 0.75,
                "text-halo-blur": 0.75
            }
        }, {
            "id": "marine_label_4",
            "type": "symbol",
            "metadata": {
                "mapbox:group": "1444849258897.3083"
            },
            "source": "mapbox",
            "source-layer": "marine_label",
            "interactive": true,
            "filter": [
                "all", [
                    "==",
                    "$type",
                    "Point"
                ],
                [
                    ">=",
                    "labelrank",
                    4
                ]
            ],
            "layout": {
                "text-font": [
                    "Open Sans Italic",
                    "Arial Unicode MS Regular"
                ],
                "text-field": "{name_en}",
                "text-max-width": 6,
                "text-letter-spacing": 0.2,
                "symbol-placement": "point",
                "text-size": {
                    "stops": [
                        [
                            3,
                            11
                        ],
                        [
                            4,
                            12
                        ]
                    ]
                }
            },
            "paint": {
                "text-color": "#74aee9",
                "text-halo-color": "rgba(255,255,255,0.7)",
                "text-halo-width": 0.75,
                "text-halo-blur": 0.75
            }
        }, {
            "id": "marine_label_line_3",
            "type": "symbol",
            "metadata": {
                "mapbox:group": "1444849258897.3083"
            },
            "source": "mapbox",
            "source-layer": "marine_label",
            "interactive": true,
            "filter": [
                "all", [
                    "==",
                    "$type",
                    "LineString"
                ],
                [
                    "==",
                    "labelrank",
                    3
                ]
            ],
            "layout": {
                "text-font": [
                    "Open Sans Italic",
                    "Arial Unicode MS Regular"
                ],
                "text-field": "{name_en}",
                "text-letter-spacing": 0.2,
                "symbol-placement": "line",
                "text-size": {
                    "stops": [
                        [
                            3,
                            11
                        ],
                        [
                            4,
                            14
                        ]
                    ]
                }
            },
            "paint": {
                "text-color": "#74aee9",
                "text-halo-color": "rgba(255,255,255,0.7)",
                "text-halo-width": 0.75,
                "text-halo-blur": 0.75
            }
        }, {
            "id": "marine_label_point_3",
            "type": "symbol",
            "metadata": {
                "mapbox:group": "1444849258897.3083"
            },
            "source": "mapbox",
            "source-layer": "marine_label",
            "interactive": true,
            "filter": [
                "all", [
                    "==",
                    "$type",
                    "Point"
                ],
                [
                    "==",
                    "labelrank",
                    3
                ]
            ],
            "layout": {
                "text-font": [
                    "Open Sans Italic",
                    "Arial Unicode MS Regular"
                ],
                "text-field": "{name_en}",
                "text-max-width": 5,
                "text-letter-spacing": 0.2,
                "symbol-placement": "point",
                "text-size": {
                    "stops": [
                        [
                            3,
                            11
                        ],
                        [
                            4,
                            14
                        ]
                    ]
                }
            },
            "paint": {
                "text-color": "#74aee9",
                "text-halo-color": "rgba(255,255,255,0.7)",
                "text-halo-width": 0.75,
                "text-halo-blur": 0.75
            }
        }, {
            "id": "marine_label_line_2",
            "type": "symbol",
            "metadata": {
                "mapbox:group": "1444849258897.3083"
            },
            "source": "mapbox",
            "source-layer": "marine_label",
            "interactive": true,
            "filter": [
                "all", [
                    "==",
                    "$type",
                    "LineString"
                ],
                [
                    "==",
                    "labelrank",
                    2
                ]
            ],
            "layout": {
                "text-font": [
                    "Open Sans Italic",
                    "Arial Unicode MS Regular"
                ],
                "text-field": "{name_en}",
                "text-letter-spacing": 0.2,
                "symbol-placement": "line",
                "text-size": {
                    "stops": [
                        [
                            3,
                            14
                        ],
                        [
                            4,
                            16
                        ]
                    ]
                }
            },
            "paint": {
                "text-color": "#74aee9",
                "text-halo-color": "rgba(255,255,255,0.7)",
                "text-halo-width": 0.75,
                "text-halo-blur": 0.75
            }
        }, {
            "id": "marine_label_point_2",
            "type": "symbol",
            "metadata": {
                "mapbox:group": "1444849258897.3083"
            },
            "source": "mapbox",
            "source-layer": "marine_label",
            "interactive": true,
            "filter": [
                "all", [
                    "==",
                    "$type",
                    "Point"
                ],
                [
                    "==",
                    "labelrank",
                    2
                ]
            ],
            "layout": {
                "text-font": [
                    "Open Sans Italic",
                    "Arial Unicode MS Regular"
                ],
                "text-field": "{name_en}",
                "text-max-width": 5,
                "text-letter-spacing": 0.2,
                "symbol-placement": "point",
                "text-size": {
                    "stops": [
                        [
                            3,
                            14
                        ],
                        [
                            4,
                            16
                        ]
                    ]
                }
            },
            "paint": {
                "text-color": "#74aee9",
                "text-halo-color": "rgba(255,255,255,0.7)",
                "text-halo-width": 0.75,
                "text-halo-blur": 0.75
            }
        }, {
            "id": "marine_label_line_1",
            "type": "symbol",
            "metadata": {
                "mapbox:group": "1444849258897.3083"
            },
            "source": "mapbox",
            "source-layer": "marine_label",
            "interactive": true,
            "filter": [
                "all", [
                    "==",
                    "$type",
                    "LineString"
                ],
                [
                    "==",
                    "labelrank",
                    1
                ]
            ],
            "layout": {
                "text-font": [
                    "Open Sans Italic",
                    "Arial Unicode MS Regular"
                ],
                "text-field": "{name_en}",
                "text-letter-spacing": 0.2,
                "symbol-placement": "line",
                "text-size": {
                    "stops": [
                        [
                            3,
                            18
                        ],
                        [
                            4,
                            22
                        ]
                    ]
                }
            },
            "paint": {
                "text-color": "#74aee9",
                "text-halo-color": "rgba(255,255,255,0.7)",
                "text-halo-width": 0.75,
                "text-halo-blur": 0.75
            }
        }, {
            "id": "marine_label_point_1",
            "type": "symbol",
            "metadata": {
                "mapbox:group": "1444849258897.3083"
            },
            "source": "mapbox",
            "source-layer": "marine_label",
            "interactive": true,
            "filter": [
                "all", [
                    "==",
                    "$type",
                    "Point"
                ],
                [
                    "==",
                    "labelrank",
                    1
                ]
            ],
            "layout": {
                "text-font": [
                    "Open Sans Italic",
                    "Arial Unicode MS Regular"
                ],
                "text-field": "{name_en}",
                "text-max-width": 5,
                "text-letter-spacing": 0.2,
                "text-line-height": 1.6,
                "symbol-placement": "point",
                "text-offset": [
                    0,
                    2.4
                ],
                "text-size": {
                    "stops": [
                        [
                            3,
                            18
                        ],
                        [
                            4,
                            22
                        ]
                    ]
                }
            },
            "paint": {
                "text-color": "#74aee9",
                "text-halo-color": "rgba(255,255,255,0.7)",
                "text-halo-width": 0.75,
                "text-halo-blur": 0.75
            }
        }, {
            "id": "country_label_4",
            "type": "symbol",
            "metadata": {
                "mapbox:group": "1444849242106.713"
            },
            "source": "mapbox",
            "source-layer": "country_label",
            "interactive": true,
            "filter": [
                ">=",
                "scalerank",
                4
            ],
            "layout": {
                "text-font": [
                    "Open Sans Bold",
                    "Arial Unicode MS Bold"
                ],
                "text-field": "{name_en}",
                "text-max-width": 6.25,
                "text-transform": "uppercase",
                "text-size": {
                    "stops": [
                        [
                            4,
                            11
                        ],
                        [
                            6,
                            15
                        ]
                    ]
                }
            },
            "paint": {
                "text-color": "#334",
                "text-halo-color": "rgba(255,255,255,0.8)",
                "text-halo-width": 2,
                "text-halo-blur": 1
            }
        }, {
            "id": "country_label_3",
            "type": "symbol",
            "metadata": {
                "mapbox:group": "1444849242106.713"
            },
            "source": "mapbox",
            "source-layer": "country_label",
            "interactive": true,
            "filter": [
                "==",
                "scalerank",
                3
            ],
            "layout": {
                "text-font": [
                    "Open Sans Bold",
                    "Arial Unicode MS Bold"
                ],
                "text-field": "{name_en}",
                "text-max-width": 6.25,
                "text-transform": "uppercase",
                "text-size": {
                    "stops": [
                        [
                            3,
                            11
                        ],
                        [
                            7,
                            17
                        ]
                    ]
                }
            },
            "paint": {
                "text-color": "#334",
                "text-halo-color": "rgba(255,255,255,0.8)",
                "text-halo-width": 2,
                "text-halo-blur": 1
            }
        }, {
            "id": "country_label_2",
            "type": "symbol",
            "metadata": {
                "mapbox:group": "1444849242106.713"
            },
            "source": "mapbox",
            "source-layer": "country_label",
            "interactive": true,
            "filter": [
                "==",
                "scalerank",
                2
            ],
            "layout": {
                "text-font": [
                    "Open Sans Bold",
                    "Arial Unicode MS Bold"
                ],
                "text-field": "{name_en}",
                "text-max-width": 6.25,
                "text-transform": "uppercase",
                "text-size": {
                    "stops": [
                        [
                            2,
                            11
                        ],
                        [
                            5,
                            17
                        ]
                    ]
                }
            },
            "paint": {
                "text-color": "#334",
                "text-halo-color": "rgba(255,255,255,0.8)",
                "text-halo-width": 2,
                "text-halo-blur": 1
            }
        }, {
            "id": "country_label_1",
            "type": "symbol",
            "metadata": {
                "mapbox:group": "1444849242106.713"
            },
            "source": "mapbox",
            "source-layer": "country_label",
            "interactive": true,
            "filter": [
                "==",
                "scalerank",
                1
            ],
            "layout": {
                "text-font": [
                    "Open Sans Bold",
                    "Arial Unicode MS Bold"
                ],
                "text-field": "{name_en}",
                "text-max-width": 6.25,
                "text-transform": "uppercase",
                "text-size": {
                    "stops": [
                        [
                            1,
                            11
                        ],
                        [
                            4,
                            17
                        ]
                    ]
                }
            },
            "paint": {
                "text-color": "#334",
                "text-halo-color": "rgba(255,255,255,0.8)",
                "text-halo-width": 2,
                "text-halo-blur": 1
            }
        },



        {
            "interactive": true,
            "minzoom": 2,
            "layout": {
                "visibility": "visible"
            },

            "filter": ["==", "map", "Water"],
            "type": "fill",
            "source": "wwu",
            "id": "Water",
            "paint": {
                "fill-opacity": 1,
                "fill-color": "hsl(172, 85%, 67%)"
            },
            "source-layer": "surface"
        }, {
            "interactive": true,
            "minzoom": 2,
            "layout": {
                "visibility": "visible"
            },
            "filter": ["==", "map", "Walkway"],
            "type": "fill",
            "source": "wwu",
            "id": "Walkway",
            "paint": {
                "fill-opacity": 1,
                "fill-color": "rgba(238,226,200,1)"
            },
            "source-layer": "surface"
        }, {
            "interactive": true,
            "minzoom": 2,
            "layout": {
                "visibility": "visible"
            },
            "filter": ["==", "map", "Track"],
            "type": "fill",
            "source": "wwu",
            "id": "Track",
            "paint": {
                "fill-opacity": 1,
                "fill-color": "rgba(191,152,149,1)"
            },
            "source-layer": "surface"
        }, {
            "interactive": true,
            "minzoom": 2,
            "layout": {
                "visibility": "visible"
            },
            "filter": ["==", "map", "Sports"],
            "type": "fill",
            "source": "wwu",
            "id": "Sports",
            "paint": {
                "fill-opacity": 1,
                "fill-color": "rgba(114,142,139,1)"
            },
            "source-layer": "surface"
        }, {
            "interactive": true,
            "minzoom": 2,
            "layout": {
                "visibility": "visible"
            },
            "filter": ["==", "map", "Road"],
            "type": "fill",
            "source": "wwu",
            "id": "Road",
            "paint": {
                "fill-opacity": 1,
                "fill-color": "rgba(178,178,178,1)"
            },
            "source-layer": "surface"
        }, {
            "interactive": true,
            "minzoom": 2,
            "layout": {
                "visibility": "visible"
            },
            "filter": ["==", "map", "Red Square"],
            "type": "fill",
            "source": "wwu",
            "id": "Red Square",
            "paint": {
                "fill-opacity": 1,
                "fill-color": "rgba(215,158,158,1)"
            },
            "source-layer": "surface"
        }, {
            "interactive": true,
            "minzoom": 2,
            "layout": {
                "visibility": "visible"
            },
            "filter": ["==", "map", "Plantings"],
            "type": "fill",
            "source": "wwu",
            "id": "Plantings",
            "paint": {
                "fill-opacity": 1,
                "fill-color": "rgba(149,179,125,1)",
                "fill-outline-color": "hsl(0, 0%, 0%)",
                "fill-pattern": "pattern-plantings"
            },
            "source-layer": "surface"
        }, {
            "interactive": true,
            "minzoom": 2,
            "layout": {
                "visibility": "visible"
            },
            "filter": ["all", ["==", "map", "Parking"]],
            "type": "fill",
            "source": "wwu",
            "id": "Parking-ALL",
            "paint": {
                "fill-opacity": 1,
                "fill-color": "rgba(191,191,191,1)"
            },
            "source-layer": "surface"
        }, {
            "interactive": true,
            "minzoom": 2,
            "layout": {
                "visibility": "visible"
            },
            "filter": ["all", ["==", "map", "Parking"],
                ["==", "parking", "Campus Services"]
            ],
            "type": "fill",
            "source": "wwu",
            "id": "Parking-CS",
            "paint": {
                "fill-opacity": 0,
                "fill-color": "rgba(247, 148, 28, 1)"
            },
            "source-layer": "surface"
        }, {
            "interactive": true,
            "minzoom": 2,
            "layout": {
                "visibility": "visible"
            },
            "filter": ["all", ["==", "map", "Parking"],
                ["==", "parking", "Faculty / Staff"]
            ],
            "type": "fill",
            "source": "wwu",
            "id": "Parking-Faculty",
            "paint": {
                "fill-opacity": 0,
                "fill-color": "rgba(109, 207, 246, 1)"
            },
            "source-layer": "surface"
        }, {
            "interactive": true,
            "minzoom": 2,
            "layout": {
                "visibility": "visible"
            },
            "filter": ["all", ["==", "map", "Parking"],
                ["==", "parking", "Other"]
            ],
            "type": "fill",
            "source": "wwu",
            "id": "Parking-Other",
            "paint": {
                "fill-opacity": 0,
                "fill-color": "rgba(191,191,191,1)"
            },
            "source-layer": "surface"
        }, {
            "interactive": true,
            "minzoom": 2,
            "layout": {
                "visibility": "visible"
            },
            "filter": ["all", ["==", "map", "Parking"],
                ["==", "parking", "Pay / Display"]
            ],
            "type": "fill",
            "source": "wwu",
            "id": "Parking-Pay",
            "paint": {
                "fill-opacity": 0,
                "fill-color": "rgba(191,191,191,1)"
            },
            "source-layer": "surface"
        }, {
            "interactive": true,
            "minzoom": 2,
            "layout": {
                "visibility": "visible"
            },
            "filter": ["all", ["==", "map", "Parking"],
                ["==", "parking", "Resident"]
            ],
            "type": "fill",
            "source": "wwu",
            "id": "Parking-Resident",
            "paint": {
                "fill-opacity": 0,
                "fill-color": "rgba(237, 28, 37, 1)"
            },
            "source-layer": "surface"
        }, {
            "interactive": true,
            "minzoom": 2,
            "layout": {
                "visibility": "visible"
            },
            "filter": ["all", ["==", "map", "Parking"],
                ["==", "parking", "Restricted"]
            ],
            "type": "fill",
            "source": "wwu",
            "id": "Parking-Restricted",
            "paint": {
                "fill-opacity": 0,
                "fill-color": "rgba(141, 198, 61, 1)"
            },
            "source-layer": "surface"
        }, {
            "interactive": true,
            "minzoom": 2,
            "layout": {
                "visibility": "visible"
            },
            "filter": ["all", ["==", "map", "Parking"],
                ["==", "parking", "Student / Overflow"]
            ],
            "type": "fill",
            "source": "wwu",
            "id": "Parking-Overflow",
            "paint": {
                "fill-opacity": 0,
                "fill-color": "rgba(255, 212, 1, 1)"
            },
            "source-layer": "surface"
        }, {
            "interactive": true,
            "minzoom": 2,
            "layout": {
                "visibility": "visible"
            },
            "filter": ["==", "map", "Other"],
            "type": "fill",
            "source": "wwu",
            "id": "Other",
            "paint": {
                "fill-opacity": 1,
                "fill-color": "rgba(171,171,171,1)"
            },
            "source-layer": "surface"
        }, {
            "interactive": true,
            "minzoom": 2,
            "layout": {
                "visibility": "visible"
            },
            "filter": ["==", "map", "Grass"],
            "type": "fill",
            "source": "wwu",
            "id": "Grass",
            "paint": {
                "fill-opacity": 1,
                "fill-color": "rgba(149,179,125,1)"
            },
            "source-layer": "surface"
        }, {
            "interactive": true,
            "minzoom": 2,
            "layout": {
                "visibility": "visible"
            },
            "filter": ["==", "map", "Forest"],
            "type": "fill",
            "source": "wwu",
            "id": "Forest",
            "paint": {
                "fill-opacity": 1,
                "fill-color": "rgba(84,128,68,1)",
                "fill-pattern": "pattern-forest"
            },
            "source-layer": "surface"
        }, {
            "interactive": true,
            "minzoom": 2,
            "layout": {
                "visibility": "visible"
            },
            "filter": ["==", "map", "Dirt"],
            "type": "fill",
            "source": "wwu",
            "id": "Dirt",
            "paint": {
                "fill-opacity": 1,
                "fill-color": "rgba(215,194,158,1)"
            },
            "source-layer": "surface"
        }, {
            "interactive": true,
            "minzoom": 2,
            "layout": {
                "visibility": "visible"
            },
            "filter": ["==", "map", "Building"],
            "type": "fill",
            "source": "wwu",
            "id": "Building",
            "paint": {
                "fill-opacity": 1,
                "fill-color": "rgba(199,176,135,1)"
            },
            "source-layer": "surface"
        }, {
            "interactive": true,
            "minzoom": 2,
            "layout": {
                "visibility": "visible"
            },
            "filter": ["==", "map", "Building"],
            "filter": [
                "all", [
                    "==",
                    "map",
                    "Building"
                ],
                ["in", "name", "Miller Hall", "Academic Instructional Center - West", "Wade King Student Recreation Center"]
            ],
            "type": "fill",
            "source": "wwu",
            "id": "LEED Buildings",
            "paint": {
                "fill-opacity": 0,
                "fill-color": "rgba(27,167,23,1)"
            },
            "source-layer": "surface"
        }, {
            "interactive": true,
            "minzoom": 2,
            "layout": {
                "visibility": "visible"
            },
            "filter": ["==", "map", "Art"],
            "type": "fill",
            "source": "wwu",
            "id": "Art",
            "paint": {
                "fill-opacity": 1,
                "fill-color": "rgba(176,131,170,1)",
                "fill-outline-color": "rgba(76,0,115,1)"
            },
            "source-layer": "surface"
        }, {
            "interactive": true,
            "minzoom": 2,
            "layout": {
                "visibility": "visible"
            },
            "filter": ["==", "type", "Bike Shelter"],
            "type": "fill",
            "source": "wwu",
            "id": "Bike Shelter",
            "paint": {
                "fill-color": "rgba(205,170,102,1)",
                "fill-outline-color": "hsl(0, 0%, 0%)",
                "fill-opacity": 1
            },
            "source-layer": "carto_polygons"
        }, {
            "interactive": true,
            "minzoom": 2,
            "layout": {
                "visibility": "none"
            },
            "type": "fill",
            "source": "wwu",
            "id": "Art Base",
            "paint": {},
            "source-layer": "art_base_polygons"
        }, {
            "interactive": true,
            "minzoom": 2,
            "layout": {
                "visibility": "visible"
            },
            "filter": ["==", "type", "Bridge"],
            "type": "fill",
            "source": "wwu",
            "id": "Bridge",
            "paint": {
                "fill-color": "rgba(205,170,102,1)",
                "fill-outline-color": "rgba(137,137,68,1)"
            },
            "source-layer": "carto_polygons"
        }, {
            "interactive": true,
            "minzoom": 2,
            "layout": {
                "visibility": "visible"
            },
            "filter": ["==", "type", "Bus Stop"],
            "type": "fill",
            "source": "wwu",
            "id": "Bus Stops",
            "paint": {
                "fill-color": "rgba(205,170,102,1)"
            },
            "source-layer": "carto_polygons"
        }, {
            "interactive": true,
            "minzoom": 2,
            "layout": {
                "visibility": "visible"
            },
            "filter": ["==", "type", "Overhang"],
            "type": "fill",
            "source": "wwu",
            "id": "Overhangs - Poly",
            "paint": {
                "fill-color": "rgba(205,170,102,1)"
            },
            "source-layer": "carto_polygons"
        }, {
            "interactive": true,
            "minzoom": 2,
            "layout": {
                "visibility": "visible"
            },
            "filter": ["==", "type", "Tunnel"],
            "type": "line",
            "source": "wwu",
            "id": "Tunnels",
            "paint": {
                "line-width": 0.8,
                "line-dasharray": [2, 3]
            },
            "source-layer": "carto_lines"
        }, {
            "interactive": true,
            "minzoom": 2,
            "layout": {
                "visibility": "visible"
            },
            "filter": ["==", "type", "Overhang"],
            "type": "line",
            "source": "wwu",
            "id": "Overhangs",
            "paint": {
                "line-width": 0.8,
                "line-dasharray": [2, 3]
            },
            "source-layer": "carto_lines"
        }, {
            "interactive": true,
            "minzoom": 2,
            "layout": {
                "visibility": "visible"
            },
            "filter": ["==", "type", "Elevation Change"],
            "type": "line",
            "source": "wwu",
            "id": "Stairs-Elevation",
            "paint": {
                "line-color": "rgba(137,90,68,1)",
                "line-width": 1.2
            },
            "source-layer": "stair_lines"
        }, {
            "interactive": true,
            "minzoom": 17,
            "layout": {
                "visibility": "visible"
            },
            "filter": ["==", "type", "Stair Tred - Alt"],
            "type": "line",
            "source": "wwu",
            "id": "Stairs-Tread-Alt",
            "paint": {
                "line-color": "rgba(205,170,102,1)",
                "line-width": {
                    "base": 1,
                    "stops": [
                        [17, 0.5],
                        [22, 1]
                    ]
                }
            },
            "source-layer": "stair_lines"
        }, {
            "interactive": true,
            "minzoom": 17,
            "layout": {
                "visibility": "visible"
            },
            "filter": ["==", "type", "Stair Tred - Main"],
            "type": "line",
            "source": "wwu",
            "id": "Stairs-Tread",
            "paint": {
                "line-color": "rgba(205,170,102,1)",
                "line-width": 0.5
            },
            "source-layer": "stair_lines"
        }, {
            "interactive": true,
            "minzoom": 15,
            "layout": {
                "visibility": "visible"
            },
            "filter": ["==", "type", "Top of Stairs"],
            "type": "line",
            "source": "wwu",
            "id": "Stairs-Top",
            "paint": {
                "line-color": "rgba(137,90,68,1)",
                "line-width": {
                    "base": 1,
                    "stops": [
                        [17, 0.85],
                        [22, 1.35]
                    ]
                },
                "line-opacity": {
                    "base": 1,
                    "stops": [
                        [15, 1],
                        [22, 1]
                    ]
                }
            },
            "source-layer": "stair_lines"
        }, {
            "interactive": true,
            "minzoom": 2,
            "layout": {
                "visibility": "visible"
            },
            "filter": ["==", "dissolve", "Walk"],
            "type": "line",
            "source": "wwu",
            "id": "Walk-Edge",
            "paint": {
                "line-width": 0.8,
                "line-color": "rgba(137,112,68,1)"
            },
            "source-layer": "curb_walk_building_lines"
        }, {
            "interactive": true,
            "minzoom": 2,
            "layout": {
                "visibility": "visible"
            },
            "filter": ["==", "dissolve", "Curb"],
            "type": "line",
            "source": "wwu",
            "id": "Curb-Edge",
            "paint": {
                "line-width": 0.8,
                "line-color": "rgba(130,130,130,1)"
            },
            "source-layer": "curb_walk_building_lines"
        }, {
            "interactive": true,
            "minzoom": 2,
            "layout": {
                "visibility": "visible"
            },
            "filter": ["==", "dissolve", "Building"],
            "type": "line",
            "source": "wwu",
            "id": "Building-Edge",
            "paint": {
                "line-width": 0.8,
                "line-color": "rgba(130,130,130,1)"
            },
            "source-layer": "curb_walk_building_lines"
        }, {
            "interactive": true,
            "minzoom": 15,
            "layout": {
                "visibility": "visible"
            },
            "filter": ["==", "type", "Main"],
            "type": "line",
            "source": "wwu",
            "id": "Parking Stalls - Main",
            "paint": {
                "line-color": "rgba(225,225,225,1)",
                "line-opacity": {
                    "base": 1,
                    "stops": [
                        [15, 0],
                        [16, 1]
                    ]
                }
            },
            "source-layer": "parking_stall_lines"
        }, {
            "interactive": true,
            "minzoom": 15,
            "layout": {
                "visibility": "visible"
            },
            "filter": ["==", "type", "Alt"],
            "type": "line",
            "source": "wwu",
            "id": "Parking Stalls - Alt",
            "paint": {
                "line-color": "rgba(225,225,225,1)",
                "line-opacity": {
                    "base": 1,
                    "stops": [
                        [15, 0],
                        [16, 1]
                    ]
                }
            },
            "source-layer": "parking_stall_lines"
        }, {
            "interactive": true,
            "minzoom": 15,
            "layout": {
                "visibility": "visible"
            },
            "filter": ["==", "type", "Bumperline"],
            "type": "line",
            "source": "wwu",
            "id": "Parking Stalls - Bumper",
            "paint": {
                "line-color": "rgba(225,225,225,1)",
                "line-opacity": 1,
                "line-width": {
                    "base": 1,
                    "stops": [
                        [16, 0.5],
                        [22, 2]
                    ]
                }
            },
            "source-layer": "parking_stall_lines"
        }, {
            "interactive": true,
            "minzoom": 15,
            "layout": {
                "visibility": "visible"
            },
            "filter": ["==", "type", "Bumper Alt"],
            "type": "line",
            "source": "wwu",
            "id": "Parking Stalls - Bumper Alt",
            "paint": {
                "line-color": "rgba(225,225,225,1)",
                "line-opacity": 0,
                "line-width": {
                    "base": 1,
                    "stops": [
                        [15, 0.2],
                        [16, 0.5],
                        [22, 2]
                    ]
                }
            },
            "source-layer": "parking_stall_lines"
        }, {
            "minzoom": 16,
            "layout": {
                "visibility": "visible"
            },
            "type": "line",
            "source": "wwu",
            "id": "Crosswalk",
            "paint": {
                "line-color": "hsl(0, 0%, 100%)",
                "line-width": {
                    "base": 1,
                    "stops": [
                        [17, 0.8],
                        [22, 34]
                    ]
                },
                "line-opacity": {
                    "base": 1,
                    "stops": [
                        [16, 0],
                        [17, 1]
                    ]
                }
            },
            "source-layer": "crosswalk_lines",
            "interactive": true
        }, {
            "minzoom": 18,
            "layout": {
                "visibility": "visible"
            },
            "filter": ["all", ["==", "level", 1],
                ["in", "Alt_Name_1", "Restroom - Mens", "Restroom - Womens"]
            ],
            "type": "fill",
            "source": "wwu",
            "id": "Restrooms",
            "paint": {
                "fill-color": "hsl(192, 54%, 42%)"
            },
            "source-layer": "rooms",
            "interactive": true
        }, {
            "minzoom": 18,
            "layout": {
                "visibility": "visible"
            },
            "metadata": {},
            "filter": ["all", ["!in", "Alt_Name", "", "Hallway", "Restroom - Mens", "Restroom - Womens", "Stairs"],
            ["!in", "Descriptio", "", "Classroom", "Elevator", "Office"],
            ["==", "level", 1]],
            "type": "fill",
            "source": "wwu",
            "id": "Rooms",
            "paint": {
                "fill-color": "hsl(72, 87%, 93%)",
                "fill-opacity": 1
            },
            "source-layer": "rooms",
            "interactive": true
        }, {
            "minzoom": 18,
            "layout": {
                "text-field": "{ROOMNUMBER}",
                "symbol-avoid-edges": true,
                "text-anchor": "center",
                "text-justify": "center"
            },
            "metadata": {},
            "filter": ["all", ["<", "ROOMNUMBER", "X00"],
                ["==", "level", 1]
            ],
            "type": "symbol",
            "source": "wwu",
            "id": "Room Labels",
            "paint": {},
            "source-layer": "rooms",
            "interactive": true
        }, {
            "minzoom": 18,
            "layout": {
                "visibility": "visible"
            },
            "filter": ["all", ["==", "Alt_Name_1", "Hallway"],
                ["==", "level", 1]
            ],
            "type": "fill",
            "source": "wwu",
            "id": "Hallways",
            "paint": {
                "fill-color": "hsl(77, 27%, 79%)"
            },
            "source-layer": "rooms",
            "interactive": true
        }, {
            "minzoom": 18,
            "layout": {
                "visibility": "visible"
            },
            "filter": ["all", ["==", "Alt_Name_1", "Stairs"],
                ["==", "level", 1]
            ],
            "type": "fill",
            "source": "wwu",
            "id": "Stairs",
            "paint": {
                "fill-color": "hsl(77, 27%, 79%)"
            },
            "source-layer": "rooms",
            "interactive": true
        }, {
            "minzoom": 18,
            "layout": {
                "visibility": "visible"
            },
            "filter": ["all", ["==", "Descript_1", "Elevator"],
                ["==", "level", 1]
            ],
            "type": "fill",
            "source": "wwu",
            "id": "Elevators",
            "paint": {
                "fill-color": "hsl(298, 67%, 76%)"
            },
            "source-layer": "rooms",
            "interactive": true
        }, {
            "minzoom": 18,
            "layout": {
                "visibility": "visible"
            },
            "filter": ["all", ["==", "Descript_1", "Office"],
                ["==", "level", 1]
            ],
            "type": "fill",
            "source": "wwu",
            "id": "Office",
            "paint": {
                "fill-color": "hsl(282, 37%, 93%)",
                "fill-opacity": 1
            },
            "source-layer": "rooms",
            "interactive": true
        }, {
            "minzoom": 18,
            "layout": {
                "visibility": "visible"
            },
            "filter": ["all", ["==", "Descript_1", "Classroom"],
                ["==", "level", 1]
            ],
            "type": "fill",
            "source": "wwu",
            "id": "Classroom",
            "paint": {
                "fill-color": "hsl(165, 9%, 67%)"
            },
            "source-layer": "rooms",
            "interactive": true
        }, {
            "minzoom": 18,
            "layout": {
                "visibility": "visible"
            },
            "filter": ["all", ["==", "BUILDINGID", "0"],
                ["==", "ROOMNUMBER", "0"],
                ["==", "level", 1]
            ],
            "type": "fill",
            "source": "wwu",
            "id": "Selected Rooms",
            "paint": {
                "fill-color": "rgb(255,127,80)"
            },
            "source-layer": "rooms",
            "interactive": true
        }, {
            "minzoom": 18,
            "layout": {
                "visibility": "visible"
            },
            "filter": ["all", ["!=", "type", "Floor"],
                ["==", "level", 1]
            ],
            "type": "fill",
            "source": "wwu",
            "id": "walls",
            "paint": {
                "fill-color": "hsl(0, 18%, 35%)"
            },
            "source-layer": "walls",
            "interactive": true
        }, {
            "minzoom": 18,
            "layout": {
                "visibility": "visible"
            },
            "filter": ["all", ["==", "level", 1],
                ["==", "type", "Floor"]
            ],
            "type": "fill",
            "source": "wwu",
            "id": "Door Jams",
            "paint": {
                "fill-color": "hsl(77, 27%, 79%)"
            },
            "source-layer": "walls",
            "interactive": true
        }, {
            "minzoom": 18,
            "layout": {
                "visibility": "visible"
            },
            "filter": ["all", ["==", "level", 1],
                ["in", "Layer", "A-DOOR", "A-Door"]
            ],
            "type": "line",
            "source": "wwu",
            "id": "Doors",
            "paint": {
                "line-color": "hsl(0, 0%, 37%)"
            },
            "source-layer": "lines",
            "interactive": true
        }, {
            "minzoom": 18,
            "layout": {
                "visibility": "visible"
            },
            "filter": ["all", ["==", "level", 1],
                ["in", "Layer", "A-STRS", "A-Strs"]
            ],
            "type": "line",
            "source": "wwu",
            "id": "Stair-Lines",
            "paint": {
                "line-color": "hsl(0, 0%, 37%)"
            },
            "source-layer": "lines",
            "interactive": true
        }, {
            "minzoom": 18,
            "layout": {
                "visibility": "visible"
            },
            "filter": ["all", ["==", "level", 1],
                ["in", "Layer", "A-FLOR-PFIX", "A-Flor-Pfix"]
            ],
            "type": "line",
            "source": "wwu",
            "id": "Plumbing",
            "paint": {
                "line-color": "hsl(215, 90%, 23%)"
            },
            "source-layer": "lines",
            "interactive": true
        }, {
            "minzoom": 15,
            "layout": {
                "text-field": "{p_lot}",
                "symbol-placement": "line",
                "symbol-avoid-edges": true,
                "icon-image": "circle-15",
                "icon-size": 2.5
            },
            "filter": ["all", ["==", "map", "Parking"],
                ["has", "p_lot"]
            ],
            "type": "symbol",
            "source": "wwu",
            "id": "Parking Anno",
            "paint": {
                "text-color": "hsl(0, 0%, 100%)"
            },
            "source-layer": "surface",
            "interactive": true
        }, {
            "minzoom": 15,
            "layout": {
                "visibility": "visible",
                "text-field": "{BUILDINGID}"
            },
            "maxzoom": 18,
            "type": "symbol",
            "source": "wwu",
            "id": "Building Anno",
            "paint": {},
            "source-layer": "building_anno",
            "interactive": true
        }, {
            "interactive": true,
            "minzoom": 15,
            "layout": {
                "visibility": "visible"
            },
            "filter": ["==", "type", "track"],
            "type": "line",
            "source": "wwu",
            "id": "Sports - Track",
            "paint": {
                "line-color": "hsl(0, 0%, 100%)",
                "line-width": {
                    "base": 1,
                    "stops": [
                        [16, 0.1],
                        [22, 0.8]
                    ]
                },
                "line-opacity": {
                    "base": 1,
                    "stops": [
                        [15, 0],
                        [16, 1]
                    ]
                }
            },
            "source-layer": "sport_lines"
        }, {
            "interactive": true,
            "minzoom": 15,
            "layout": {
                "visibility": "visible"
            },
            "filter": ["in", "type", "soccer 1", "soccer 2"],
            "type": "line",
            "source": "wwu",
            "id": "Sports - Soccer",
            "paint": {
                "line-color": "hsl(0, 0%, 100%)",
                "line-width": {
                    "base": 1,
                    "stops": [
                        [16, 0.5],
                        [22, 2]
                    ]
                },
                "line-opacity": {
                    "base": 1,
                    "stops": [
                        [15, 0],
                        [16, 1]
                    ]
                }
            },
            "source-layer": "sport_lines"
        }, {
            "interactive": true,
            "minzoom": 15,
            "layout": {
                "visibility": "visible"
            },
            "filter": ["==", "type", "jump board"],
            "type": "line",
            "source": "wwu",
            "id": "Sports - Jump Board",
            "paint": {
                "line-color": "hsl(0, 32%, 60%)",
                "line-width": {
                    "base": 1.6,
                    "stops": [
                        [17, 3],
                        [22, 16.75]
                    ]
                },
                "line-opacity": {
                    "base": 1,
                    "stops": [
                        [15, 0],
                        [16, 1]
                    ]
                }
            },
            "source-layer": "sport_lines"
        }, {
            "interactive": true,
            "minzoom": 15,
            "layout": {
                "visibility": "visible"
            },
            "filter": ["==", "type", "runway"],
            "type": "line",
            "source": "wwu",
            "id": "Sports - Runway",
            "paint": {
                "line-color": "hsl(0, 0%, 100%)",
                "line-width": 0.8,
                "line-opacity": {
                    "base": 1,
                    "stops": [
                        [15, 0],
                        [16, 1]
                    ]
                }
            },
            "source-layer": "sport_lines"
        }, {
            "interactive": true,
            "minzoom": 15,
            "layout": {
                "visibility": "visible"
            },
            "filter": ["==", "type", "baseball"],
            "type": "line",
            "source": "wwu",
            "id": "Sports - Baseball",
            "paint": {
                "line-width": {
                    "base": 1,
                    "stops": [
                        [16, 0.5],
                        [22, 2]
                    ]
                },
                "line-color": "hsl(0, 0%, 100%)",
                "line-opacity": {
                    "base": 1,
                    "stops": [
                        [15, 0],
                        [16, 1]
                    ]
                }
            },
            "source-layer": "sport_lines"
        }, {
            "interactive": true,
            "minzoom": 15,
            "layout": {
                "visibility": "visible"
            },
            "filter": ["==", "type", "Basketball"],
            "type": "line",
            "source": "wwu",
            "id": "Sports - Basketball",
            "paint": {
                "line-color": "hsl(0, 0%, 100%)",
                "line-width": {
                    "base": 1,
                    "stops": [
                        [16, 0.5],
                        [22, 2]
                    ]
                },
                "line-opacity": {
                    "base": 1,
                    "stops": [
                        [15, 0],
                        [16, 1]
                    ]
                }
            },
            "source-layer": "sport_lines"
        }, {
            "interactive": true,
            "minzoom": 15,
            "layout": {
                "visibility": "visible"
            },
            "filter": ["in", "type", "Tennis", "Tennis RW"],
            "type": "line",
            "source": "wwu",
            "id": "Sports - Tennis",
            "paint": {
                "line-color": "hsl(0, 0%, 100%)",
                "line-width": {
                    "base": 1,
                    "stops": [
                        [16, 0.5],
                        [22, 2]
                    ]
                },
                "line-opacity": {
                    "base": 1,
                    "stops": [
                        [15, 0],
                        [16, 1]
                    ]
                }
            },
            "source-layer": "sport_lines"
        }, {
            "interactive": true,
            "minzoom": 15,
            "layout": {
                "visibility": "visible"
            },
            "filter": ["==", "type", "biology"],
            "type": "line",
            "source": "wwu",
            "id": "Sports - Biology",
            "paint": {
                "line-color": "hsl(0, 0%, 100%)",
                "line-width": {
                    "base": 1,
                    "stops": [
                        [16, 0.5],
                        [22, 2]
                    ]
                },
                "line-opacity": {
                    "base": 1,
                    "stops": [
                        [15, 0],
                        [16, 1]
                    ]
                }
            },
            "source-layer": "sport_lines"
        }, {
            "interactive": true,
            "minzoom": 15,
            "layout": {
                "visibility": "visible"
            },
            "filter": ["==", "type", "Other"],
            "type": "line",
            "source": "wwu",
            "id": "Sports - Other",
            "paint": {
                "line-color": "hsl(0, 0%, 100%)",
                "line-width": {
                    "base": 1,
                    "stops": [
                        [16, 0.5],
                        [22, 2]
                    ]
                },
                "line-opacity": {
                    "base": 1,
                    "stops": [
                        [15, 0],
                        [16, 1]
                    ]
                }
            },
            "source-layer": "sport_lines"
        }, {
            "interactive": true,
            "minzoom": 2,
            "layout": {
                "visibility": "none"
            },
            "type": "circle",
            "source": "wwu",
            "id": "Accessible Curbs",
            "paint": {},
            "source-layer": "acc_curb"
        }, {
            "interactive": true,
            "minzoom": 18,
            "layout": {
                "visibility": "none"
            },
            "filter": ["all", ["==", "type", "Acc GN Restroom"],
                ["in", "floorlevel", "1", "1 2", "1 2 3", "1 2 3 4 5", "1 2 3 4 5 6", "1 3", "? All", "? LL1-2     1 2", "All"]
            ],
            "type": "circle",
            "source": "wwu",
            "id": "Gender Neutral Restroom - Accessible",
            "paint": {},
            "source-layer": "acc_features"
        }, {
            "interactive": true,
            "minzoom": 18,
            "layout": {
                "visibility": "none"
            },
            "filter": ["all", ["==", "type", "Gender Neutral Restroom NA"],
                ["in", "floorlevel", "1", "1 2", "1 2 3", "1 2 3 4 5", "1 2 3 4 5 6", "1 3", "? All", "? LL1-2     1 2", "All"]
            ],
            "type": "circle",
            "source": "wwu",
            "id": "Gender Neutral Restroom - Not Accessible",
            "paint": {},
            "source-layer": "acc_features"
        }, {
            "interactive": true,
            "minzoom": 18,
            "layout": {
                "visibility": "none"
            },
            "filter": ["==", "type", "Acc Restroom"],
            "type": "circle",
            "source": "wwu",
            "id": "Restroom - Accessible",
            "paint": {},
            "source-layer": "acc_features"
        }, {
            "interactive": true,
            "minzoom": 18,
            "layout": {
                "visibility": "none"
            },
            "filter": ["all", ["==", "type", "Lift"],
                ["in", "floorlevel", "0 1 2", "0 1 2 3 4 5 6 7 8", "1", "1 2", "1 2 3", "1 2 3 4", "1 2 3 4 5", "1 2 3 4 5 6", "1 3", "? All", "? LL1-2     1 2", "All"]
            ],
            "type": "circle",
            "source": "wwu",
            "id": "Lifts",
            "paint": {},
            "source-layer": "acc_features"
        }, {
            "interactive": true,
            "minzoom": 2,
            "layout": {
                "icon-image": "accessible-manual",
                "icon-size": {
                    "base": 1,
                    "stops": [
                        [16, 0.5],
                        [22, 1]
                    ]
                }
            },
            "filter": ["==", "type", "Accessible Manual"],
            "type": "symbol",
            "source": "wwu",
            "id": "Manual Activated",
            "paint": {
                "icon-opacity": 0.9
            },
            "source-layer": "acc_features"
        }, {
            "interactive": true,
            "minzoom": 2,
            "layout": {
                "icon-image": "accessible-automatic",
                "icon-allow-overlap": true,
                "icon-ignore-placement": true,
                "icon-size": {
                    "base": 1,
                    "stops": [
                        [16, 0.5],
                        [22, 1]
                    ]
                }
            },
            "filter": ["==", "type", "Button Activated"],
            "type": "symbol",
            "source": "wwu",
            "id": "Button Activated",
            "paint": {
                "icon-opacity": 1
            },
            "source-layer": "acc_features"
        }, {
            "minzoom": 15,
            "layout": {
                "visibility": "none",
                "icon-image": "accessible-parking",
                "icon-allow-overlap": true,
                "icon-rotation-alignment": "viewport",
                "icon-size": {
                    "base": 1,
                    "stops": [
                        [16, 0.5],
                        [22, 1]
                    ]
                }
            },
            "filter": ["==", "type", "Accessible"],
            "type": "symbol",
            "source": "wwu",
            "id": "Accessible Parking",
            "paint": {},
            "source-layer": "acc_parking",
            "interactive": true
        }, {
            "minzoom": 15,
            "layout": {
                "visibility": "none",
                "icon-image": "accessible-van",
                "icon-allow-overlap": true,
                "icon-rotation-alignment": "viewport",
                "icon-size": {
                    "base": 1,
                    "stops": [
                        [16, 0.5],
                        [22, 1]
                    ]
                }
            },
            "filter": ["in", "type", "Accessible & Van Accessible", "Van Accessible"],
            "type": "symbol",
            "source": "wwu",
            "id": "Accessible Van",
            "paint": {},
            "source-layer": "acc_parking",
            "interactive": true
        }, {
            "interactive": true,
            "minzoom": 2,
            "layout": {
                "visibility": "none"
            },
            "type": "line",
            "source": "wwu",
            "id": "Self Guided tour",
            "paint": {},
            "source-layer": "self_guided_tour"
        }, {
            "minzoom": 2,
            "layout": {
                "visibility": "none"
            },
            "type": "circle",
            "source": "wwu",
            "id": "Self Guided Points",
            "paint": {},
            "source-layer": "self_guided_points",
            "interactive": true
        }, {
            "minzoom": 2,
            "layout": {
                "visibility": "none"
            },
            "filter": ["==", "type", "Emergency Phone"],
            "type": "circle",
            "source": "wwu",
            "id": "Emergency Phones",
            "paint": {},
            "source-layer": "phones",
            "interactive": true
        }, {
            "minzoom": 16,
            "layout": {
                "visibility": "none",
                "line-join": "round"
            },
            "maxzoom": 18,
            "type": "line",
            "source": "wwu",
            "id": "Carto Lines",
            "paint": {
                "line-dasharray": [2.5, 1.5]
            },
            "source-layer": "carto_lines",
            "interactive": true
        }
    ]
};

export default style;