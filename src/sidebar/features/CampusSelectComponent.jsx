import React from 'react';
import campusList from './campuslist.js';
import './campusSelect.less';

class CampusSelectComponent extends React.Component {

    constructor(props) {
        super(props);
        this.state = {value: 'main'};
        this.handleChange = this.handleChange.bind(this);
    }

    render() {
        let optionsDOM = campusList.map((campus) => {
            let name = campus.name;
            return (
                <option key={name} value={name}>{name}</option>
            );
        });

        return (
            <div id="container-choose-campus">
                <span id="label-choose-campus">Choose Campus</span>
                <div className="styled-select">
                    <select id="control-choose-campus" value={this.state.value} onChange={this.handleChange}>
                        {optionsDOM}
                    </select>
                </div>
            </div>
        );
    }

    handleChange(event) {
        this.setState({value: event.target.value});
        let bounding = campusList.find(function(campus) {
            return campus.name === event.target.value;
        }).bounding;
        this.props.zoomToBox(bounding);
    }

}

export default CampusSelectComponent;
