import React from 'react';
import CampusSelectComponent from './CampusSelectComponent.jsx';
import LayerGroupComponent from './LayerGroupComponent.jsx';
import './features.less';

const accesbilityLayers = [
    {name: 'Accessible Parking', icon: 'icon-accessible-parking'},
    {name: 'Accessible Van Parking', icon: 'icon-accessible-van'},
    {name: 'Automatic Doors', icon: 'icon-accessible-automatic'},
    {name: 'Manual Doors', icon: 'icon-accessible-manual'},
    {name: 'Routes', icon: 'icon-accessible-route'},
    {name: 'Imppassible', icon: 'icon-accessible-impassible'},
    {name: 'Curb Cuts', icon: 'icon-accessible-curb-cuts'},
    {name: 'Elevators / Lifts', icon: 'icon-accessible-lifts'}
];

const techLayers = [
    {name: 'ATUS Computer Labs', icon: 'icon-tech-computer-lab'},
    {name: 'Res Tek Computer Labs', icon: 'icon-tech-restek-lab'},
    {name: 'Computer Help', icon: 'icon-tech-computer-help'},
    {name: 'Wifi', icon: 'icon-tech-wifi'}
];

const foodLayers = [
    {name: 'Residential', icon: 'icon-food-residential'},
    {name: 'Retail / Markets', icon: 'icon-food-retail'}
];

const transportationLayers = [
    {name: 'WTA Stops', icon: 'icon-transportation-wta-stop'},
    {name: 'WTA Routes', icon: 'icon-transporation-wta-routes'},
    {name: 'Late Night Stops', icon: 'icon-transportation-late-night-stop'},
    {name: 'Late Night Routes', icon: 'icon-transportation-late-night-route'},
    {name: 'Bike Racks', icon: 'icon-transportation-bike-racks'},
    {name: 'Bike Routes', icon: 'icon-transportation-bike-routes'},
    {name: 'Bike Walk Zones', icon: 'icon-transportation-bike-walk-zones'},
    {name: 'Bike Repair', icon: 'icon-transportation-bike-repair'}
];

const sustainabilityLayers = [
    {name: 'Sustainability Features', icon: 'icon-sustainability-features'},
    {name: 'Leed Buildings', icon: 'icon-sustainability-leed-building'}
];

const constructionLayers = [
    {name: 'Construction Projects', icon: 'icon-construction-point'},
    {name: 'Construction Area', icon: 'icon-construction-area'}
];

const safetyLayers = [
    {name: 'Emergency Phones', icon: 'icon-safety-emergency-phone'},
    {name: 'Outdoor Campus Courtesy Phones', icon: 'icon-safety-courtesy-phone'},
    {name: 'Campus Police', icon: 'icon-safety-campus-police'},
    {name: 'Major Disaster Assembly Area', icon: 'icon-safety-major-disaster-assembly-area'},
    {name: 'Lighted Walkways', icon: 'icon-safety-lighted-walkway'}
];

const parkingLayers = [
    {name: 'Faculty & Staff', icon: 'icon-parking-faculty'},
    {name: 'Restricted', icon: 'icon-parking-restricted'},
    {name: 'Student Commuter', icon: 'icon-parking-student-commuter'},
    {name: 'Residents', icon: 'icon-parking-residents'},
    {name: 'Campus Services', icon: 'icon-parking-campus-services'}
];

const floorplanLayers = [
    {name: 'Offices', icon: 'icon-floorplans-office'},
    //{name: 'Department Offices', icon: ''},
    {name: 'Classrooms', icon: 'icon-floorplans-classroom'},
    //{name: 'Computer Labs', icon: ''},
    //{name: 'Teaching Labs', icon: ''},
    //{name: 'Research Labs', icon: ''}
];

class FeaturesComponent extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div id="features" className={this.props.visibleComponent === 'features' ? '' : 'hidden'}>
                <CampusSelectComponent 
                    zoomToBox={this.props.zoomToBox}
                />
                <LayerGroupComponent 
                    name="Accessibility"
                    changeLayerVisibility={this.props.changeLayerVisibility}
                    layers={accesbilityLayers}
                />
                <LayerGroupComponent 
                    name="Tech"
                    changeLayerVisibility={this.props.changeLayerVisibility}
                    layers={techLayers}
                />
                <LayerGroupComponent 
                    name="Food"
                    changeLayerVisibility={this.props.changeLayerVisibility}
                    layers={foodLayers}
                />
                <LayerGroupComponent 
                    name="Transportation"
                    changeLayerVisibility={this.props.changeLayerVisibility}
                    layers={transportationLayers}
                />
                <LayerGroupComponent 
                    name="Sustainability"
                    changeLayerVisibility={this.props.changeLayerVisibility}
                    layers={sustainabilityLayers}
                />
                <LayerGroupComponent 
                    name="Construction"
                    changeLayerVisibility={this.props.changeLayerVisibility}
                    layers={constructionLayers}
                />
                <LayerGroupComponent 
                    name="Safety"
                    changeLayerVisibility={this.props.changeLayerVisibility}
                    layers={safetyLayers}
                />
                <LayerGroupComponent 
                    name="Parking"
                    changeLayerVisibility={this.props.changeLayerVisibility}
                    layers={parkingLayers}
                />
                <LayerGroupComponent 
                    name="Floorplans"
                    changeLayerVisibility={this.props.changeLayerVisibility}
                    layers={floorplanLayers}
                />
            </div>
        );
    }
}

export default FeaturesComponent;
