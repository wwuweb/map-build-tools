import json

buildings_json_path = r'../data/buildings.json'
buildings_json = open(buildings_json_path, 'r')
buildings_json = json.load(buildings_json)

rooms_json_path = r'../data/rooms.json'
rooms_json = open(rooms_json_path, 'r')
rooms_json = json.load(rooms_json)

for building in buildings_json:
	print building

	building_rooms = [x for x in rooms_json['features'] if x['properties']['BUILDINGID'] == building['abbrev']]
	print building_rooms
