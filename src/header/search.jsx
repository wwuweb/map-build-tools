import React from 'react';
import './search.less';

class SearchComponent extends React.Component {

    constructor(props) {
        super(props);
        this.state = {};
        this.clearSearch = this.clearSearch.bind(this);
    }

    clearSearch() {
        this.props.updateSearchFilter({target: {value: ''}});
        this.refs.search.value = '';
    }

    render() {
        return ( 
            <div id="search-bar">
                <input type="text" 
                    ref="search"
                    placeholder="Search Map"
                    defaultValue={this.props.filterText}
                    onChange={this.props.updateSearchFilter} />
                <span className="close fa fa-close" onClick={this.clearSearch}></span>
            </div>
        );
    }

}

export default SearchComponent;