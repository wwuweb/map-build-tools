import React from 'react';
import './layer.less';

class LayerComponent extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div>
                <span className={'icon ' + this.props.icon}></span> {this.props.name}
            </div>
        );
    }

}

export default LayerComponent;
