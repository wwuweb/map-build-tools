import React from 'react';
import FeaturesComponent from './sidebar/features/FeaturesComponent.jsx';
import SearchComponent from './sidebar/search/searchComponent.jsx';
import './sidebar.less';

class SidebarComponent extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            
        };
    }

    render() {
        return ( 
            <div id="sidebar">
                <FeaturesComponent
                    visibleComponent={this.props.visibleSidebarPane}
                    changeLayerVisibility={this.props.changeLayerVisibility}
                    zoomToBox={this.props.zoomToBox}
                />
                <div id="attributes"></div>
                <SearchComponent
                    visibleComponent={this.props.visibleSidebarPane}
                    updateSearchFilter={this.props.updateSearchFilter}
                    filterText={this.props.filterText}
                    zoomTo={this.props.zoomTo}
                />
            </div>
        );
    }

}

export default SidebarComponent;