# Installing and Running on Windows #

Some of the dependencies don't work on Windows.  To get around this, we'll install the 
Windows Linux Subsystem (WSL).  You can do that with the following steps.

1. Go to the Start Menu and type "Windows Powershell".

2. Right-click "Windows Powershell" and select "Run as Administrator".

3. Run the following command:

	$ Enable-WindowsOptionalFeature -Online -FeatureName Microsoft-Windows-Subsystem-Linux

4. Install the Ubuntu for Windows Linux Subsystem with the following command:

5. Open the Windows Store and search for Ubuntu 18.04.  Click "Get" to install.

6. Click the "Launch" button after installing.

7. When prompted, create a linux username or password.

8. Run the following command to update the distro and install needed dependencies:

```sh
$ sudo apt update && sudo apt upgrade
$ sudo apt-get install nodejs npm libsqlite3-dev zliblg-dev
$ sudo apt-get install python-setuptools python-dev
```

9. Instal gdal and dependencies

```sh
$ sudo add-apt-repository ppa:ubuntugis/ppa && sudo apt-get updateg
$ sudo apt-get install gdal-bin
```

## Building the map ##

The WSL mounts your windows drives in the /mnt folder.  On your Windows machine, using the GitHub app, clone the following repos into the same folder that has this project.

	a. https://github.com/mapbox/mbutil
	b. https://github.com/mapbox/tippecanoe

Your folder structure should look something like this:

	campus_map
		map-build-tools
		mb-util
		tippecanoe

From your Linux command prompt, browse to the campus_map folder.  e.g. cd /mnt/c/projects/campus_map

cd into the mb-util folder and running the following commands to build the mb-util program.

	$ sudo python setup.py install

cd into the tippecanoe folder and run the following commands to build tippecanoe:
	
```sh
$ make -j
$ make install
```

cd into the map-build-tools folder and run the following commands to prepare the project

/usr/bin/npm install

Make png images from svg icons for use in map sprites

```sh
$ node tools/svgIconsToPNG.js
```

Export geodatabase layers to geojson files.  Make sure to copy the geodatabase to data/geoData/gdb/wwu_campus_map.gdb.

```sh
$ /usr/bin/npm run gdbToVector
```

* There are lots of non-fatal errors that occur during the process, which does take some time to complete.  When done, you should delete any geojson files in the data/geoData/geojson that you do not want included in the vector tiles.  Eventually, the GDB could/should be cleaned up or be an export of the master GDB that only contains layers that should go into the map.

Generate Vector tiles

```sh
$ /usr/bin/npm run generateTiles
```

Once all the data is prepped, we can run the project.

```sh
$ /usr/bin/npm run start
```

This launches a small web server and builds all the output files.  You can access the map from your browser at http://localhost:9001