import React from 'react';

class FloorButtonComponent extends React.Component {

    constructor(props) {
        super(props);
        this.state = {};
        this.changeFloor = this.changeFloor.bind(this);
    }

    changeFloor() {
        this.props.changeFloor(this.props.floor);
    }

    render() {
        let current = this.props.currentFloor === this.props.floor;
        let label = this.props.floor ;
        if (label === 0) {
            label = 'B';
        }
        return ( 
            <button 
                className="floorbutton" 
                key={this.props.floor}
                data-current={current}
                onClick={this.changeFloor} >
                {label}
            </button>
        );
    }

}

export default FloorButtonComponent;