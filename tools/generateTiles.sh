#!/usr/bin/env bash
mkdir -p data/geoData/tiles/
rm -Rf data/geoData/tiles/wwu.mbtiles
tippecanoe data/geoData/geojson/*.json -B14 -Z2 -z 20 -o data/geoData/tiles/wwu.mbtiles
rm -Rf dist/tiles/wwu
mkdir dist/tiles
mb-util --image_format=pbf data/geoData/tiles/wwu.mbtiles dist/tiles/wwu

cp htaccess.txt dist/tiles/wwu/.htaccess