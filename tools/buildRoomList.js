const fs = require('fs');
const turf = require('turf');

let buildings = JSON.parse(fs.readFileSync('../data/buildings.json', 'utf8'));
let roomsJSON = JSON.parse(fs.readFileSync('../data/rooms.json', 'utf8'));
let rooms = roomsJSON.features;

function trimDecimals(number) {
    return Math.round(number * 100000) / 100000;
}

for (let idx in buildings) {
    let building = buildings[idx];
    let name = building.name;
    let abbrev = building.abbrev;

    let building_rooms = rooms.filter(function(room) {
        if (room.properties.BUILDINGID === abbrev && room.properties.ROOMNUMBER) {
            return true;
        }
    }).map(function(room) {
        room.centroid = turf.centroid(room.geometry).geometry.coordinates;
        room.centroid = [trimDecimals(room.centroid[0]), trimDecimals(room.centroid[1])];
        return {
                floor: room.properties.FLOORCODE,
                level: room.properties.level,
                room: room.properties.ROOMNUMBER,
                coords: room.centroid
            };
    }).sort(function(a, b) {
        /* Sort by level, and then room number */
        if (a.level > b.level) {
            return 1;
        } else if (a.level < b.level) {
            return -1;
        } else {
            if (a.room > b.room) {
                return 1;
            } else if (a.room < b.room) {
                return -1;
            }
            return 0;
        }
        
    });

    buildings[idx].rooms = building_rooms;
}

var output = 'const buildings = ' + ${JSON.stringify(buildings)} + '; export default buildings;';

fs.writeFile('../src/sidebar/search/data/buildings.json', output, 'utf8');
