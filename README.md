# README #

This repository contains the code for processing the spatial data and building the website for the Western Washington University campus map [https://www.wwu.edu/map](Link URL).

## How do I get set up? ##

### On Mac and Linux ###

No special install steps are needed on *nix systems.  All the dependencies will work out of the box.

### Installing Dependencies ###

1. Download and install node on your system from [https://nodejs.org/en/](Link URL).

2. clone this repository:

		$ git clone git@bitbucket.org:wwuweb/map-build-tools.git

3. Browse to the root directory and run

		$ npm install

this will install all of the required dependencies

4. To build the website run

		$ npm run dev

5. Browse to the dist directory in your browser to see the built version of the map

## Building map data ##

The geospatial data comes from many sources within WWU and is updated frequently.  We have built several tools to help process this data into useable forms for the campus map.  These tools are located in the tools folder.  We will update this readme with details on those tools soon.

## Adding map symbols ##

Create an svg of the icon you wish to use.  At the moment, the dimensions of the icon need to be square.  Place the icon in the /src/icons folder.  To generate standard and retina pngs from the icon (mapbox only supports png icons at the moment), run

		$ npm run generateTiles

Webpack will automatically generate image sprites when you build the application.  The icons can now be used in the mapbox style sheet by referencing the icon name.  You may use the icons in css by setting the class to "icon icon-<icon-name>".

## Generating map tiles ##

We use Mapbox's TippeCanoe to generate vector tiles from our campus geojson files.  TippeCanoe is easiest
installed on a mac using Brew with 

		$ brew install tippecanoe

on other 'nix systems, tippecanoe will need to be compiled.  For up to date instructions see the TippeCanoe
git repository at https://github.com/mapbox/tippecanoe

Once TippeCanoe is installed, run

		$ npm run generateTiles

## Who do I talk to? ##

* Jacob Lesser jacob@prusikmapping.com
* Stefan Freelan stefan.freelan@wwu.edu