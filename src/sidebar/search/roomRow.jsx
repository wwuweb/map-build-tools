import React from 'react';

class RoomRowComponent extends React.Component {

    constructor(props) {
        super(props);
        //this.state = {value: 'main'};
        this.handleClick = this.handleClick.bind(this);
    }

    render() {
        return (
            <div
                onClick={this.handleClick}
                className='room'>
                {this.props.room.room}
            </div>
        );
    }

    handleClick() {
        console.log(this);
        this.props.zoomTo(this.props.room.coords, 18);
    }

}

export default RoomRowComponent;
