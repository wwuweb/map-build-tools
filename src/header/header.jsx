import React from 'react';
import SearchComponent from './search.jsx';
import './header.less';

class HeaderComponent extends React.Component {

    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        return ( 
            <div id="header">
                <div id="logo"><a href="https://www.wwu.edu">Western Washington University</a></div>
                <SearchComponent 
                    filterText={this.props.filterText}
                    updateSearchFilter={this.props.updateSearchFilter}
                />
            </div>
        );
    }

}

export default HeaderComponent;