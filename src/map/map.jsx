import React, {Component} from 'react';
import ReactMapboxGl, { Feature, Layer, ZoomControl } from 'react-mapbox-gl';
import FloorSelector from './floorselector/floorselector.jsx';
import { Map } from 'immutable';
import config from './config.js';
import style from './mapstyle/style.js';
import { debounce } from 'lodash';
import './map.less';

const { accessToken } = config;

const containerStyle = {
    height: '100%',
    width: '100%'
};

const MapElement = ReactMapboxGl({ accessToken });

class MapComponent extends React.Component {

    constructor(props) {
        super(props);
        this.minFloorZoom = 18;
        this.state = {
            campus: new Map()
        };
        this.maxZoom = 20;
        this.minZoom = 1;

        this._onZoomClick = this._onZoomClick.bind(this);
        this._onZoomEnd = this._onZoomEnd.bind(this);
        this._onMoveEnd = this._onMoveEnd.bind(this);
        this._onStyleLoad = this._onStyleLoad.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        const { currentFloor, sustainabilityVisibility, parkingVisibility } = nextProps;
        if (this.map && this.map.getZoom() >= this.minFloorZoom && currentFloor !== undefined) {
            this.setFloorLevel(currentFloor);
        }

        //this.map.setPaintProperty('LEED Buildings', 'fill-opacity', sustainabilityVisibility);
        this.map.setPaintProperty('Parking-CS', 'fill-opacity', parkingVisibility);
        this.map.setPaintProperty('Parking-Faculty', 'fill-opacity', parkingVisibility);
        this.map.setPaintProperty('Parking-Resident', 'fill-opacity', parkingVisibility);
        this.map.setPaintProperty('Parking-Restricted', 'fill-opacity', parkingVisibility);
        this.map.setPaintProperty('Parking-Overflow', 'fill-opacity', parkingVisibility);
    }

    _onZoomClick(map, zoomDiff) {
        const zoom = map.getZoom() + zoomDiff;
        if (zoom >= this.minZoom && zoom <= this.maxZoom) {
            this.props.setZoom(zoom);
        }
    }

    _onMoveEnd(map) {
        this.getFloorsInView(map);
    }

    _onZoomEnd(map) {
        this.getFloorsInView(map);
    }

    _onStyleLoad(map) {
        let _this = this;
        map.on('tiledata',
            debounce(function() {
                _this.getFloorsInView(map);
            }, 400, {leading: true, trailing: true})
        );
        this.map = map;
        this.props.setMap(map);
    }

    getFloorsInView(map) {
        if (map.getZoom() >= this.minFloorZoom) {

            // Get all rooms that are within the vector tiles intersecting the viewport
            // Does not limit features that fall outside of the viewport
            let rooms = map.querySourceFeatures('wwu', {sourceLayer: 'rooms'});

            // Get unique floor numbers
            let floors = Array.from(new Set(rooms.map(function(room) {
                let floor = parseInt(room.properties.FLOORCODE);
                if (floor) {
                    return floor;
                }
                return null;
            })));

            let max = Math.max.apply(null, floors);
            let min =  Math.min.apply(null, floors);
            if (max > -1) {
                this.setState({maxfloor: max, minfloor: min});
            } else {
                this.setState({maxfloor: null, minfloor: null});
            }
        } else {
            this.setState({maxfloor: null, minfloor: null});
        }

    }

    setFloorLevel(level) {
        const map = this.map;
        map.setFilter('Rooms', ['all', ['!in', 'Alt_Name', '', 'Hallway', 'Restroom - Mens', 'Restroom - Womens', 'Stairs'],
            ['!in', 'Descriptio', '', 'Classroom', 'Elevator', 'Office'],
            ['==', 'level', level]
        ]);
        map.setFilter('Room Labels', ['==', 'level', level]);
        map.setFilter('Hallways', ['all', ['==', 'Alt_Name_1', 'Hallway'],
            ['==', 'level', level]
        ]);
        map.setFilter('Stairs', ['all', ['==', 'Alt_Name_1', 'Stairs'],
            ['==', 'level', level]
        ]);
        map.setFilter('Elevators', ['all', ['==', 'Descript_1', 'Elevator'],
            ['==', 'level', level]
        ]);
        map.setFilter('Office', ['all', ['==', 'Descript_1', 'Office'],
            ['==', 'level', level]
        ]);
        map.setFilter('Classroom', ['all', ['==', 'Descript_1', 'Classroom'],
            ['==', 'level', level]
        ]);
        map.setFilter('Selected Rooms', ['all', ['==', 'BUILDINGID', '0'],
            ['==', 'ROOMNUMBER', '0'],
            ['==', 'level', level]
        ]);
        map.setFilter('walls', ['all', ['!=', 'type', 'Floor'],
            ['==', 'level', level]
        ]);
        map.setFilter('Door Jams', ['all', ['==', 'level', level],
            ['==', 'type', 'Floor']
        ]);
        map.setFilter('Doors', ['all', ['==', 'level', level],
            ['in', 'Layer', 'A-DOOR', 'A-Door']
        ]);
        map.setFilter('Stair-Lines', ['all', ['==', 'level', level],
            ['in', 'Layer', 'A-STRS', 'A-Strs']
        ]);
        map.setFilter('Plumbing', ['all', ['==', 'level', level],
            ['in', 'Layer', 'A-FLOR-PFIX', 'A-Flor-Pfix']
        ]);

    }

    render() {
        return ( 
            <div id='map'>
                <MapElement
                    style={style}
                    center={this.props.center}
                    zoom={this.props.zoom}
                    maxZoom={this.maxZoom}
                    accessToken={accessToken}
                    containerStyle={containerStyle}
                    onMoveEnd={this._onMoveEnd} 
                    onZoomEnd={this._onZoomEnd}
                    onStyleLoad={this._onStyleLoad}
                >

                    <ZoomControl 
                        zoomDiff={1}
                        onControlClick={this._onZoomClick} />

                    <FloorSelector 
                        maxfloor={this.state.maxfloor}
                        minfloor={this.state.minfloor}
                        currentFloor={this.props.currentFloor}
                        changeFloor={this.props.changeFloor}
                    />

                    {/* Accessbiility Layers */}
                    <Layer
                        id='Walkway-NotAccessible'
                        type='fill'
                        paint={{
                            'fill-opacity': this.props.accessibilityVisibility,
                            'fill-color': 'hsl(0, 66%, 65%)'
                        }}
                        layerOptions={{
                            source:'wwu',
                            'source-layer':'surface',
                            'filter': ['all', ['==', 'map', 'Walkway'],
                                ['in', 'accessibility', 'No', 'STAIRS']
                            ],
                            'minzoom': 2,
                        }}
                    />

                    <Layer
                        id='Accessible Curbs'
                        type='circle'
                        paint={{
                            'circle-opacity': this.props.accessibilityVisibility,
                            'circle-color': 'rgb(0, 47, 221)',
                            'circle-radius': {
                                'base': 1.3,
                                'stops': [
                                    [
                                        13,
                                        1
                                    ],
                                    [
                                        20,
                                        6
                                    ]
                                ]
                            }
                        }}
                        layerOptions={{
                            source:'wwu',
                            'source-layer':'acc_curb',
                            'minzoom': 2,
                        }}
                    />

                    <Layer
                        id='Manual Activated'
                        type='symbol'
                        layout={{
                            'icon-image': 'accessible-manual',
                            'icon-size': {
                                'base': 1,
                                'stops': [
                                    [16, 0.5],
                                    [22, 1]
                                ]
                            }
                        }}
                        paint={{
                            'icon-opacity': this.props.accessibilityVisibility
                        }}
                        layerOptions={{
                            source:'wwu',
                            'source-layer':'acc_features',
                            'minzoom': 2,
                            'filter': ['==', 'type', 'Accessible Manual'],
                        }}
                    />

                    <Layer
                        id='Button Activated'
                        type='symbol'
                        layout={{
                            'icon-image': 'accessible-automatic',
                            'icon-size': {
                                'base': 1,
                                'stops': [
                                    [16, 0.5],
                                    [22, 1]
                                ]
                            }
                        }}
                        paint={{
                            'icon-opacity': this.props.accessibilityVisibility
                        }}
                        layerOptions={{
                            source:'wwu',
                            'source-layer':'acc_features',
                            minzoom: 2,
                            filter: ['==', 'type', 'Button Activated'],
                        }}
                    />

                    <Layer
                        id='Accessible Routes'
                        type='line'
                        paint={{
                            'line-opacity': this.props.accessibilityVisibility,
                            'line-width': 2,
                            'line-color': 'rgba(0,112,255,1)',
                            'line-dasharray': [2, 2]
                        }}
                        layerOptions={{
                            source:'wwu',
                            'source-layer':'acc_routes',
                            minzoom: 2
                        }}
                    />

                    {/* Tech Layers */}

                    <Layer
                        id='ATUS Labs - Rooms'
                        type='symbol'
                        layout={{
                            'icon-image': 'tech-computer-lab',
                            'icon-size': {
                                'base': 1,
                                'stops': [
                                    [16, 0.5],
                                    [18, 1]
                                ]
                            }
                        }}
                        paint={{
                            'icon-opacity': this.props.techVisibility
                        }}
                        layerOptions={{
                            source:'wwu',
                            'source-layer':'tech',
                            minzoom: 2,
                            filter: ['all', ['==', 'floor_leve', '1'],
                                ['==', 'type', 'ATUS Computer Lab']
                            ]
                        }}
                    />

                    <Layer
                        id='ATUS Labs - Buildings'
                        type='symbol'
                        layout={{
                            'icon-image': 'tech-computer-lab',
                            'icon-size': {
                                'base': 1,
                                'stops': [
                                    [18, 1],
                                    [22, 1.5]
                                ]
                            }
                        }}
                        paint={{
                            'icon-opacity': this.props.techVisibility
                        }}
                        layerOptions={{
                            source:'wwu',
                            'source-layer':'tech',
                            minzoom: 2,
                            maxzoom: 17,
                            filter: ['==', 'type', 'Multiple ATUS Computer Labs']
                        }}
                    />

                    <Layer
                        id='Computer Help'
                        type='symbol'
                        layout={{
                            'icon-image': 'tech-computer-help',
                            'icon-size': {
                                'base': 1,
                                'stops': [
                                    [18, 1],
                                    [22, 2]
                                ]
                            }
                        }}
                        paint={{
                            'icon-opacity': this.props.techVisibility
                        }}
                        layerOptions={{
                            source:'wwu',
                            'source-layer':'tech',
                            minzoom: 2,
                            maxzoom: 17,
                            filter: ['==', 'type', 'Computer Help']
                        }}
                    />

                    <Layer
                        id='Wifi'
                        type='fill'
                        paint={{
                            'fill-opacity': this.props.techVisibility ? 0.5 : 0,
                            'fill-color': 'rgb(0, 166, 255)'
                        }}
                        layerOptions={{
                            source:'wwu',
                            'source-layer':'wifi',
                            minzoom: 2,
                            maxzoom: 22,
                        }}
                    />

                    {/* Food Layers */}

                     <Layer
                        id='Food Residential'
                        type='symbol'
                        layout={{
                            'icon-image': 'food-residential',
                            'icon-size': {
                                'base': 1,
                                'stops': [
                                    [2, 0.5],
                                    [20, 1]
                                ]
                            }
                        }}
                        paint={{
                            'icon-opacity': this.props.foodVisibility
                        }}
                        layerOptions={{
                            source:'wwu',
                            'source-layer':'food',
                            minzoom: 2,
                            filter: ['==', 'type', 'residential']
                        }}
                    />

                    <Layer
                        id='Food Retail'
                        type='symbol'
                        layout={{
                            'icon-image': 'food-retail',
                            'icon-size': {
                                'base': 1,
                                'stops': [
                                    [2, 0.5],
                                    [20, 1]
                                ]
                            }
                        }}
                        paint={{
                            'icon-opacity': this.props.foodVisibility
                        }}
                        layerOptions={{
                            source:'wwu',
                            'source-layer':'food',
                            minzoom: 2,
                            filter: ['==', 'type', 'retail']
                        }}
                    />

                    {/* Transportation Layers */}

                    <Layer
                        id='Bike Walk Zones'
                        type='fill'
                        paint={{
                            'fill-opacity': this.props.techVisibility ? 0.5 : 0,
                            'fill-color': 'rgb(226, 220, 74)'
                        }}
                        layerOptions={{
                            source:'wwu',
                            'source-layer':'bike_walk_zones',
                            minzoom: 15,
                            maxzoom: 22,
                        }}
                    />

                    <Layer
                        id='WTA Routes'
                        type='line'
                        paint={{
                            'line-opacity': this.props.transportationVisibility,
                            'line-width': 2,
                            'line-color': 'rgba(194,106,48,1)',
                        }}
                        layerOptions={{
                            source:'wwu',
                            'source-layer':'wta_routes',
                            minzoom: 2
                        }}
                    />

                    <Layer
                        id='Late Night Routes'
                        type='line'
                        paint={{
                            'line-opacity': this.props.transportationVisibility,
                            'line-width': 2,
                            'line-color': 'rgba(0,95,255,1)',
                        }}
                        layerOptions={{
                            source:'wwu',
                            'source-layer':'late_night_routes',
                            minzoom: 2
                        }}
                    />

                    <Layer
                        id='WTA Stops'
                        type='symbol'
                        layout={{
                            'icon-image': 'transportation-wta-stop',
                            'icon-size': {
                                'base': 1,
                                'stops': [
                                    [2, 0.5],
                                    [20, 1]
                                ]
                            }
                        }}
                        paint={{
                            'icon-opacity': this.props.transportationVisibility
                        }}
                        layerOptions={{
                            source:'wwu',
                            'source-layer':'bus_stops_wta',
                            minzoom: 2,
                        }}
                    />

                    <Layer
                        id='Late Night Stops'
                        type='symbol'
                        layout={{
                            'icon-image': 'transportation-late-night-stop',
                            'icon-size': {
                                'base': 1,
                                'stops': [
                                    [2, 0.5],
                                    [20, 1]
                                ]
                            }
                        }}
                        paint={{
                            'icon-opacity': this.props.transportationVisibility
                        }}
                        layerOptions={{
                            source:'wwu',
                            'source-layer':'bus_stops',
                            minzoom: 2,
                        }}
                    />

                    <Layer
                        id='Bike Racks'
                        type='symbol'
                        layout={{
                            'icon-image': 'transportation-bike-racks',
                            'icon-size': {
                                'base': 1,
                                'stops': [
                                    [2, 0.5],
                                    [20, 1]
                                ]
                            }
                        }}
                        paint={{
                            'icon-opacity': this.props.transportationVisibility
                        }}
                        layerOptions={{
                            source:'wwu',
                            'source-layer':'bike_racks',
                            minzoom: 2,
                        }}
                    />

                    <Layer
                        id='Bike Repair'
                        type='symbol'
                        layout={{
                            'icon-image': 'transportation-bike-repair',
                            'icon-size': {
                                'base': 1,
                                'stops': [
                                    [2, 0.5],
                                    [20, 1]
                                ]
                            }
                        }}
                        paint={{
                            'icon-opacity': this.props.transportationVisibility
                        }}
                        layerOptions={{
                            source:'wwu',
                            'source-layer':'bike_repair',
                            minzoom: 2,
                        }}
                    />

                    <Layer
                        id='Bike Routes'
                        type='line'
                        paint={{
                            'line-opacity': this.props.transportationVisibility,
                            'line-width': 2,
                            'line-dasharray': [2, 2],
                            'line-color': 'rgba(11, 130, 106, 1)',
                        }}
                        layerOptions={{
                            source:'wwu',
                            'source-layer':'bike_routes',
                            minzoom: 15
                        }}
                    />

                    {/* Sustainability Layers */}

                    <Layer
                        id='Sustainable Features'
                        type='circle'
                        paint={{
                            'circle-opacity': this.props.sustainabilityVisibility,
                            'circle-color': 'rgb(0, 128, 0)',
                            'circle-radius': {
                                'base': 1.3,
                                'stops': [
                                    [
                                        13,
                                        1
                                    ],
                                    [
                                        20,
                                        6
                                    ]
                                ]
                            }
                        }}
                        layerOptions={{
                            source:'wwu',
                            'source-layer':'sustainable',
                            'minzoom': 14,
                        }}
                    />


                </MapElement>
            </div>
        );
    }

}

export default MapComponent;