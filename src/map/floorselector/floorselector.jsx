import React from 'react';
import {range} from 'lodash';
import FloorButton from './floorbutton.jsx';
import './floorselector.less';

class FloorSelectorComponent extends React.Component {

    constructor(props) {
        super(props);
        this.state = {};
        this.buildFloorButtons = this.buildFloorButtons.bind(this);
    }

    buildFloorButtons () {
        let _this = this;
        let maxfloor = this.props.maxfloor;
        let minfloor = this.props.minfloor;
        if (maxfloor) {
            let floors = range(maxfloor, minfloor - 1);
            let floorButtonsDOM = floors.map(function(floor) {
                return <FloorButton 
                    key={floor} 
                    floor={floor} 
                    currentFloor={_this.props.currentFloor}
                    changeFloor={_this.props.changeFloor} />;
            });
            return floorButtonsDOM;
        }
    }

    render() {
        let floorButtons = this.buildFloorButtons();
        return ( 
            <div id="floor-select" data-current-floor={this.props.currentfloor}>
                {floorButtons}
            </div>
        );
    }

}

export default FloorSelectorComponent;