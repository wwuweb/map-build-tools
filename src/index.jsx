import React from 'react';
import {render} from 'react-dom';
import SidebarComponent from './sidebar.jsx';
import MapComponent from './map/map.jsx';
import HeaderComponent from './header/header.jsx';
import './index.less';

class App extends React.Component {

    constructor (props) {
        super(props);
        this.state = {
            Accessibility: 0,
            Tech: 0,
            Food: 0,
            Transportation: 0,
            Sustainability: 0,
            Parking: 0,
            visibleSidebarPane: 'features',
            filterText: '',
            center: [-122.487044, 48.732304],
            zoom: [15],
            currentFloor: 1
        };
        this.changeLayerVisibility = this.changeLayerVisibility.bind(this);
        this.changeFloor = this.changeFloor.bind(this);
        this.updateSearchFilter = this.updateSearchFilter.bind(this);
        this.zoomTo = this.zoomTo.bind(this);
        this.setZoom = this.setZoom.bind(this);
        this.zoomToBox = this.zoomToBox.bind(this);
        this.setMap = this.setMap.bind(this);
    }

    changeLayerVisibility (layer, opacity) {
        this.setState({[layer]: opacity});
    }

    changeFloor(floor) {
        this.setState({currentFloor: floor});
    }

    updateSearchFilter(e) {
        const term = e.target.value;
        
        // TODO: visibleSidebarPane should be set in only one location to prevent
        // fighting functions
        this.setState({
            filterText: term,
            visibleSidebarPane: term.length > 0 ? 'search' : 'features'
        });

        if (!term) {
            document.querySelectorAll('#search-bar input')[0].value = '';
        }
    }

    zoomTo(coords, zoom) {
        this.setState({
            zoom: [zoom],
            center: coords
        });
    }

    zoomToBox(bounds, maxZoom) {
        this.map.fitBounds(bounds);
    }

    setMap(map) {
        this.map = map;
    }

    setZoom(zoom) {
        this.setState({zoom: [zoom]});
    }

    render () {
        return (
            <div id="container">
                <HeaderComponent 
                    filterText={this.state.filterText}
                    updateSearchFilter={this.updateSearchFilter}
                />
                <MapComponent 
                    center={this.state.center}
                    zoom={this.state.zoom}
                    accessibilityVisibility={this.state.Accessibility}
                    techVisibility={this.state.Tech}
                    foodVisibility={this.state.Food}
                    transportationVisibility={this.state.Transportation}
                    sustainabilityVisibility={this.state.Sustainability}
                    parkingVisibility={this.state.Parking}
                    setZoom={this.setZoom}
                    changeFloor={this.changeFloor}
                    currentFloor={this.state.currentFloor}
                    setMap={this.setMap}
                />
                <SidebarComponent 
                    visibleSidebarPane={this.state.visibleSidebarPane}
                    changeLayerVisibility={this.changeLayerVisibility}
                    updateSearchFilter={this.updateSearchFilter}
                    filterText={this.state.filterText}
                    zoomTo={this.zoomTo}
                    zoomToBox={this.zoomToBox}
                />
            </div>
        );
    
    }
}

render(<App/>, document.getElementById('app'));