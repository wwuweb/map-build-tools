const svg_to_png = require('svg-to-png');
const fs = require('fs');
const path = require('path');
const svgFolder = '../src/icons/';

process.chdir('tools');

let svgpaths = [];

if (!fs.existsSync(svgFolder + 'png')){
    fs.mkdirSync(svgFolder + 'png');
}

fs.readdirSync(svgFolder + 'png').forEach(function(file) {
    let curPath = svgFolder + 'png/' + file;
    fs.unlinkSync(curPath);
});

fs.readdirSync(svgFolder).forEach(file => {
    let extension = path.extname(file);
    if (extension === '.svg') {
        // TODO: This is ridiculous.  Clearly I don't know how to handle paths
        svgpaths.push(path.resolve(path.relative(path.resolve('.'), svgFolder + file)));
    }

});

svg_to_png.convert(svgpaths, svgFolder + '/png', {
    defaultWidth: '40px',
    defaultHeight: '40px',
    compress: true
}).then(function(){
    fs.readdir(svgFolder + 'png', (err, files) => {
        files.forEach(file => {
            fs.rename(svgFolder + 'png/' + file,
                svgFolder + 'png/' + file.split('.')[0] + '@2x' + '.png');
        });
    });

    svg_to_png.convert(svgpaths, svgFolder + '/png', {
        defaultWidth: '20px',
        defaultHeight: '20px',
        compress: true
    });

});


