import React from 'react';
import Layer from './layerComponent.jsx';
import './layerGroup.less';

class LayerSelectComponent extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            name: props.name,
            is_checked: false,
            layers: props.layers
        };
    }

    buildLayers() {
        let layersDOM = this.state.layers.map((layer) => {
            return (
                <Layer key={layer.name}
                    name={layer.name} 
                    icon={layer.icon} />
            );
        });
        return layersDOM;
    }

    toggleCheckbox() {
        this.setState({
            is_checked: !this.state.is_checked
        });
        this.props.changeLayerVisibility(this.props.name, !this.state.is_checked ? 1 : 0);
    }

    render() {
        let legend = this.buildLayers();
        return (
            <div className="feature-row">
                <label className="feature">
                    <input type="checkbox" 
                        checked={this.state.is_checked}
                        onChange={this.toggleCheckbox.bind(this)}
                    />
                    <span className="checkbox"></span>
                    <span className="feature-label">{this.state.name}</span>
                </label>
                <div className="legend" data-open={this.state.is_checked}>
                    {legend}
                </div>
            </div>
        );
    }

}

export default LayerSelectComponent;
