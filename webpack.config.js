const ExtractTextPlugin = require('extract-text-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const webpack = require('webpack');
const path = require('path');
const SpritesmithPlugin = require('webpack-spritesmith');


/* Helpful resources
https://www.codementor.io/tamizhvendan/tutorials/
beginner-guide-setup-reactjs-environment-npm-babel-6-webpack-du107r9zr
*/

var BUILD_DIR = path.resolve(__dirname, 'dist');
var SRC_DIR = path.resolve(__dirname, 'src');
const DEBUG = process.env.NODE_ENV !== 'production';

var spriteJsonTemplRetina = function(data) {
    var sprites = {};
    data.sprites.map(function(sprite) {
        sprites[sprite.name] = {
            "width": sprite.width * 2,
            "height": sprite.height * 2,
            "x": Math.abs(sprite.offset_x) * 2,
            "y": Math.abs(sprite.offset_y) * 2,
            "pixelRatio": 2
        };
    });

    return JSON.stringify(sprites);
};

var spriteJsonTemplStandard = function(data) {
    var sprites = {};
    data.sprites.map(function(sprite) {
        sprites[sprite.name] = {
            "width": sprite.width,
            "height": sprite.height,
            "x": Math.abs(sprite.offset_x),
            "y": Math.abs(sprite.offset_y),
            "pixelRatio": 1
        };
    });

    return JSON.stringify(sprites);
};

var spriteLessTempl = function(data) {
    let less = '';
    data.sprites.map(function(sprite) {
        less += '.icon-' + sprite.name + ' {\n' +
                  '.retina-sprite(@' + sprite.name + '-group)\n' +
                '}\n\n';
    });
    return less;
}

const config = {
    devtool: DEBUG ? 'eval' : false,
    devServer: {
        port: 9001,
        compress: true,
        before: function(app) {
            app.get('/dist/tiles/*', function(req, res, next) {
                res.header('Content-Encoding', 'gzip');
                next();
            });
        }
    },
    entry: {
        app: path.resolve(SRC_DIR, 'index.jsx'),
        vendor: [
            'jquery',
            'react',
            'react-mapbox-gl',
            'mapbox-gl',
        ],
    },
    mode: 'development',
    output: {
        path: BUILD_DIR,
        filename: '[name].[hash].js',
    },
    resolve: {
        modules: ['src', 'node_modules'],
        alias: {
            app: path.resolve(SRC_DIR, 'index.jsx')
        },
    },

    module: {
        rules: [
            {
                test: /\.jsx?/,
                include: SRC_DIR,
                use: 'babel-loader',
                exclude: /node_modules/
            }, {
                test: /\.less$/,
                use: [
                    'style-loader',
                    'css-loader',
                    {
                        loader: 'less-loader',
                        options: {
                            javascriptEnabled: true
                        }
                    }
                ]
            }, {
                test: /\.styl$/,
                use: [
                    'style-loader',
                    'css-loader',
                    'stylus-loader'
                ]
            }, {
                test: /\.css$/,
                use: [
                    'style-loader',
                    'css-loader',
                ]
            }, { 
                test: /\.hbs$/,
                loader: 'handlebars-loader'
            }, {
                test: /\src\icons\png\retina\.svg$/,
                use: [
                    'file-loader?name=i/[hash].[ext]'
                ]
            }, {
                test: /\src\icons\png\standard\.svg$/,
                use: [
                    'file-loader?name=i/[hash].[ext]'
                ]
            }, {
                test: /\.png$/,
                use: 'url-loader?mimetype=image/png'
            }, {
                test: /\.svg$/,
                use: 'file-loader?mimetype=image/svg'
            }, {
                test: /\.woff(\?v=\d+\.\d+\.\d+)?$/,
                use: "url-loader?limit=10000&mimetype=application/font-woff"
            }, {
                test: /\.woff2(\?v=\d+\.\d+\.\d+)?$/,
                use: "url-loader?limit=10000&mimetype=application/font-woff"
            }, {
                test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
                use: "url-loader?limit=10000&mimetype=application/octet-stream"
            }, {
                test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
                use: "file-loader"
            }

        ],
    },
    plugins: [
        new webpack.LoaderOptionsPlugin({
            minimize: true
        }),
        new HtmlWebpackPlugin({
            filename: 'index.html',
            template: path.resolve(__dirname, 'src', 'index.hbs'),
            inject: false,
            alwaysWriteToDisk: true,
            path: !DEBUG ? '/dist' : '',
        }),
        /*new webpack.optimize.CommonsChunkPlugin({
            name: 'vendor',
            filename: '[name].[hash].js',
            minChunks: Infinity,
        }),*/
        new webpack.NamedModulesPlugin(),
        /*new CleanWebpackPlugin(['dist'], {
            allowExternal: true,
        }),*/
        //new ExtractTextPlugin('app.[hash].css'),
        new SpritesmithPlugin({
            src: {
                cwd: path.resolve(__dirname, 'src/icons/png'),
                glob: '**/*.png'
            },
            target: {
                image: path.resolve(__dirname, 'src/sprites/sprite.png'),
                css: [
                    [path.resolve(__dirname, 'src/sprites/sprite.less'), {
                        format: 'less'
                    }],
                    [path.resolve(__dirname, 'src/sprites/sprite.json'), {
                        format: 'spriteTempl'
                    }],
                    [path.resolve(__dirname, 'src/sprites/sprite@2x.json'), {
                        format: 'spriteTempl@2x'
                    }],
                    [path.resolve(__dirname, 'src/sprites/icons.less'), {
                        format: 'icons'
                    }]
                ]
            },
            customTemplates: {
                'spriteTempl_retina': spriteJsonTemplStandard,
                'spriteTempl@2x_retina': spriteJsonTemplRetina,
                'icons_retina': spriteLessTempl
                //'spriteStandardTempl': spriteJsonTemplStandard
            },
            apiOptions: {
                cssImageRef: "sprite.png"
            },
            retina: '@2x'
        })
    ],
    
};

module.exports = config;
