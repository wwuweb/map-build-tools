﻿// (C) Copyright 2014-2015 by Jacob Lesser
//
using System;
using System.Collections.Generic;
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;

namespace IterateObjects
{
    public class Commands
    {
        [CommandMethod("CleanGeometry")]
        static public void CleanGeometry()
        {
            Document doc = Application.DocumentManager.MdiActiveDocument;
            Editor ed = doc.Editor;
            Database db = doc.Database;

            //PromptEntityResult per = ed.GetEntity("Select a polyline");
            PromptSelectionResult acSSPrompt = ed.GetSelection();
            {

                if (acSSPrompt.Status == PromptStatus.OK)
                {
                    SelectionSet acSSet = acSSPrompt.Value;
                    foreach (SelectedObject acSSObj in acSSet)
                    {
                        // Check to make sure a valid SelectedObject object was returned
                        if (acSSObj != null)
                        {
                            Transaction tr = db.TransactionManager.StartTransaction();
                            using (tr)
                            {
                                DBObject obj = tr.GetObject(acSSObj.ObjectId, OpenMode.ForWrite);

                                // If a "lightweight" (or optimized) polyline
                                Polyline lwp = obj as Polyline;
                                if (lwp != null && lwp.Closed)
                                {
                                    // Use a for loop to get each vertex, one by one
                                    int vn = lwp.NumberOfVertices;

                                    //Only clean geometry if there are more than two vertices.
                                    //This fixes problems with circles that have only two points
                                    if (vn <= 2)
                                    {
                                        continue;
                                    }

                                    //List to keep track of vertices to remove
                                    List<int> verticesToRemove = new List<int>();

                                    //0.012 inches
                                    double threshold = 0.001;

                                    for (int i = 0; i < vn; i++)
                                    {
                                        Point2d ptA = lwp.GetPoint2dAt(i);
                                    }

                                    for (int i = 0; i < vn; i++)
                                    {

                                        Point2d ptA;
                                        Point2d ptB;
                                        Double ptABulge;
                                        Double ptBBulge;
                                       
                                        //ed.WriteMessage("\npoint " + lwp.GetPoint2dAt(i).ToString() + " bulge: " + lwp.GetBulgeAt(i));
                                        
                                        // Get the two points to compare
                                        // We will always check the point to the left
                                        // and remove the current point if the distance
                                        // between the two points is less than our threshold distance
                                        if (i == 0)
                                        {
                                            //See if the first point and last point are duplicated
                                            ptA = lwp.GetPoint2dAt(i);
                                            ptB = lwp.GetPoint2dAt(vn - 1);
                                            // Skip removing the point if it is part of a curve.
                                            // A more sophisticated analysis at a later date would determine if the point helps
                                            // determine the angle of the curve, or if it can be removed and the curve will maintain
                                            // integrity.
                                            // Also, if two points are within the threshold set as duplciate points, remove the one point
                                            // that isn't part of a curve and preserve the one point that is.
                                            // Question: Does the sign on the value of buldge represent the direction to the next point or
                                            // is it the concavity
                                            ptABulge = lwp.GetBulgeAt(i);
                                            ptBBulge = lwp.GetBulgeAt(vn - 1);
                                        }
                                        else
                                        {
                                            ptA = lwp.GetPoint2dAt(i);
                                            ptB = lwp.GetPoint2dAt(i - 1);
                                            ptABulge = lwp.GetBulgeAt(i);
                                            ptBBulge = lwp.GetBulgeAt(i - 1);
                                        }

                                        if (ptABulge != 0 || ptBBulge != 0)
                                        {
                                            continue;
                                        }


                                        // Calculate the distance between two points using
                                        double dist = ((ptA[0] - ptB[0]) * (ptA[0] - ptB[0]) + (ptA[1] - ptB[1]) * (ptA[1] - ptB[1]));

                                        if (dist < threshold)
                                        {
                                            ed.WriteMessage("\n" + "Removing Duplicate Point: " + ptA.ToString());
                                            verticesToRemove.Add(i);
                                        }

                                    }

                                    //Remove vertices
                                    for (int i = verticesToRemove.Count - 1; i > -1; i--)
                                    {
                                        lwp.RemoveVertexAt(verticesToRemove[i]);
                                    };

                                    // Use a for loop to get each vertex, one by one
                                    vn = vn - verticesToRemove.Count;

                                    verticesToRemove = new List<int>();

                                    for (int i = 0; i < vn; i++)
                                    {
                                        // Identify  the previous point
                                        Point2d nextPt;
                                        Point2d prevPt;
                                        Double nextPtBuldge;
                                        Double prevPtBuldge;

                                        // Identify the current point
                                        Point2d currentPt = lwp.GetPoint2dAt(i);
                                        Double currentPtBuldge = lwp.GetBulgeAt(i);

                                        if (i == 0)
                                        {
                                            prevPt = lwp.GetPoint2dAt(vn - 1);
                                            prevPtBuldge = lwp.GetBulgeAt(vn - 1);
                                            
                                        }
                                        else
                                        {
                                            prevPt = lwp.GetPoint2dAt(i - 1);
                                            prevPtBuldge = lwp.GetBulgeAt(i - 1);

                                        }

                                        // Identify the next point
                                        if (i == vn - 1)
                                        {
                                            nextPt = lwp.GetPoint2dAt(0);
                                            nextPtBuldge = lwp.GetBulgeAt(0);
                                        }
                                        else
                                        {
                                            nextPt = lwp.GetPoint2dAt(i + 1);
                                            nextPtBuldge = lwp.GetBulgeAt(i + 1);
                                        }


                                        if (nextPtBuldge != 0 || prevPtBuldge != 0 || currentPtBuldge != 0)
                                        {
                                            ed.WriteMessage("\nSkipping points: " + prevPt.ToString() + ", " + nextPt.ToString() + " because it is part of a curve");
                                            ed.WriteMessage(nextPtBuldge + " " + prevPtBuldge);
                                            continue;
                                        };



                                        double slopeLeft = (currentPt[1] - prevPt[1]) / (currentPt[0] - prevPt[0]);
                                        double slopeRight = (currentPt[1] - nextPt[1]) / (currentPt[0] - nextPt[0]);

                                       /* ed.WriteMessage("\n" + "Previous Point: " + prevPt.ToString());
                                        ed.WriteMessage("\n" + "Current Point: " + currentPt.ToString());
                                        ed.WriteMessage("\n" + "Slope Left: " + slopeLeft);
                                        ed.WriteMessage("\n" + "Slope Right: " + slopeRight);
                                        ed.WriteMessage("\n");*/

                                        //vertical slopes are represented as infinity
                                        //Horizontal slopes are 0
                                        //Infinity and 0 are virtually the same number according to the computer
                                        //Lots of checks to make sure we remove extra vertices on near vertical/horizontal slopes
                                        //A better method for a rewrite would be to consider every slope > 179.9999 as vertical
                                        //and every slope very close to zero as horizontal

                                        if (slopeLeft == 0 && double.IsInfinity(slopeRight))
                                        {
                                            continue;
                                        }

                                        if (slopeRight == 0 && double.IsInfinity(slopeLeft))
                                        {
                                            continue;
                                        }

                                        if (double.IsInfinity(slopeLeft) && double.IsInfinity(slopeRight))
                                        {
                                            ed.WriteMessage("\n" + "Removing Unneeded Point: " + currentPt.ToString());
                                            verticesToRemove.Add(i);
                                            continue;
                                        }

                                        if (slopeLeft > 179.999 && double.IsInfinity(slopeRight))
                                        {
                                            ed.WriteMessage("\n" + "Removing Unneeded Point: " + currentPt.ToString());
                                            verticesToRemove.Add(i);
                                            continue;
                                        }

                                        if (slopeRight > 179.999 && double.IsInfinity(slopeLeft))
                                        {
                                            ed.WriteMessage("\n" + "Removing Unneeded Point: " + currentPt.ToString());
                                            verticesToRemove.Add(i);
                                            continue;
                                        }

                                        if (slopeRight > 179.999 && slopeLeft > 179.999)
                                        {
                                            ed.WriteMessage("\n" + "Removing Unneeded Point: " + currentPt.ToString());
                                            verticesToRemove.Add(i);
                                            continue;
                                        }

                                        // Virtually equivalent doubles might not equate as  being equal due to varying precision
                                        // Here we define the tolerance as .001%
                                        //double difference = Math.Abs(slopeLeft * .00001);
                                        //ed.WriteMessage("\nDifference is " + difference.ToString());

                                        double difference = .000001;

                                        if (Math.Abs(slopeLeft - slopeRight) <= difference)
                                        {
                                            ed.WriteMessage("\n" + "Removing Unneeded Point: " + currentPt.ToString());
                                            verticesToRemove.Add(i);
                                        }

                                    }

                                    //Remove vertices
                                    for (int i = verticesToRemove.Count - 1; i > -1; i--)
                                    {
                                        lwp.RemoveVertexAt(verticesToRemove[i]);
                                    };

                                }
                                else
                                {
                                    ed.WriteMessage("\nPolyline is either not closed or is not a 'lightweight' polyline");
                                }
                                // Save the changes
                                tr.Commit();
                            }
                        }
                    }
                }
            }
        }
    }
}
