# AutoCAD Clean Geometry Plugin

This plugin for Autocad removes extraneous points along a path.  AutoCAD often inserts points that are unneccessary in defining the geometry of that path.  This often occurs at path intersections.  Sometimes duplicate points are placed at the same coordinate of a path.  This creates issues in GIS software which often cannot handle duplicate points within a line or polygon.  Extraneous points along a path are not strictly a probblem for GIS, but do increase the size of the geometry, which can be expensive in terms of geographical computations and rendering, particularly when used in vector tiles over the web.

This plugin eliminates unneeded points in two ways.  First, it checks for points within the same coordinate space and will remove all but one duplicate point.  Second, it iterates across everypoint along a path.  It calculates the slope of the line defined by the points on either side of itself.  If the slope to the left of the point in question is equal to the slope on the right -- wihthin a certain defined tolerance -- the point is removed.  This is repeated for each point in the path.

## Compiling
This plugin was written using C# in Visual Studio 2012 and tested in AutoDesk's AutoCad Architecture 2013 software.  It was compiled against the .NET 4 Framework.  It has not been tested against different software versions or compilers.

## Installation
The AutoCAD Clean Geometry.dll should be placed in the AutoCAD Program Directory.

## Using
In AutoCAD, type the cleangeometry command.  Highlight the geometry of interest and select enter.  The list of removed points, if any, will be shown in the AutoCAD terminal window.

## Tests
A CAD file with sample geometries with bad or extraneous geometries can be found in the tests folder.

## TODO
There are still some geometries that occasisionally cause the tool problems that can remove points that should not be removed.  Curves are problematic.  They can be drawn either as a series of many points, or as an AutoCAD defined curve.  The code currently does not have curve specific logic, and can remove curves if the straightline slope between the two endpoints of the curve is equal on both sides of the point in question.  For now, the code will skip any point that is part of a curve definition.  Later improvements will check for duplicate or unneeded points along a curve.