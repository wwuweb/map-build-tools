const campuslist = [{
	"name": "Main Campus",
	"center": [-122.487044, 48.732304],
	"bounding": [[-122.492023, 48.726360], [-122.482195, 48.740965]]
}, {
	"name": "Peninsula College",
	"center": [-123.413, 48.100],
	"bounding": [[-123.418350, 48.098461], [-123.406763, 48.103391]]
}, {
	"name": "Shannon Point Marine Center",
	"center": [-122.684, 48.507],
	"bounding": [[-122.691021, 48.502332], [-122.679477, 48.509355]]
}, {
	"name": "Lakewood",
	"center": [-122.341245, 48.728916],
	"bounding": [[-122.343847, 48.72713], [-122.338364, 48.731000]]
}, {
	"name": "Viqueen Lodge",
	"center": [-122.697858, 48.617735],
	"bounding": [[-122.698392, 48.617424], [-122.697324, 48.618048]]
}];

export default campuslist;