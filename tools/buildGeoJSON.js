var shapefile = require("shapefile");
const fs = require('fs');

let surface = shapefile.read('../data/geoData/shapefiles/surface.shp')
	.then(function(geojson) {
		fs.writeFile('../data/geoData/geojson/surface.json', JSON.stringify(geojson), 'utf8');
		console.log(geojson);
	});
