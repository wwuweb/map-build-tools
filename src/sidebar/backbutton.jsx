import React from 'react';
import './backbutton.less';
import 'font-awesome/css/font-awesome.css';

class BackButtonComponent extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            
        };
    }

    render() {
        return ( 
            <button onClick={this.props.onPress}>
                <span className="back-arrow fa fa-arrow-left"></span>
                <span className="text">Back</span>
            </button>
        );
    }

}

export default BackButtonComponent;
