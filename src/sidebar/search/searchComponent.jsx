import React from 'react';
import './search.less';
import BuildingData from './data/buildings.js';
import DepartmentData from './data/departments.js';
import RoomRowComponent from './roomRow.jsx';
import BackButtonComponent from '../backbutton.jsx';

class SearchComponent extends React.Component {

    constructor(props) {
        super(props);
        this.filterBuildings = this.filterBuildings.bind(this);
        this.filterRooms = this.filterRooms.bind(this);
        this.buildBuildingResults = this.buildBuildingResults.bind(this);
        this.clearSearch = this.clearSearch.bind(this);
    }

    filterRooms(building) {
        const filterText = this.props.filterText.replace(/\s+/g, '').toLowerCase();
        let room = filterText.match(/\d+.*/);
        if (room && room.length > 0) {
            room = room[0];
        } else {
            return;
        }
        return building.rooms.filter(function (v) {
            if (v.room) {
                return (v.room.toLowerCase()).search(room) !== -1;
            }
        });
    }

    filterBuildings() {
        const filterText = this.props.filterText.replace(/\s+/g, '').toLowerCase();
        const building = filterText.split(/\d+.*/)[0];
        return BuildingData.filter(function(v) {
            return v.name.replace(/\s+/g, '').toLowerCase().search(building) !== -1 ||
                v.abbrev.toLowerCase().search(building) !== -1;
        });
    }

    filterDepartments() {
        const filterText = this.props.filterText.replace(/\s+/g, '').toLowerCase();
        return DepartmentData.filter(function(v) {
            return v.dept.toLowerCase().search(filterText) !== -1;
        });
    }

    buildRoomResults(building) {
        let filteredRooms = this.filterRooms(building);
        if (filteredRooms) {
            let roomsDOM = filteredRooms.map((room) => {
                return (
                    <RoomRowComponent 
                        key={building + room.room}
                        room={room}
                        zoomTo={this.props.zoomTo}
                    />
                );
            });
            return roomsDOM;
        }
        
    }

    buildBuildingResults() {
        let buildingsDOM = this.filterBuildings().map((building) => {
            let roomsDOM = this.buildRoomResults(building);

            return (
                <div key={building.abbrev}
                    className="building">
                    {building.name}
                    {roomsDOM}
                </div>
            );
        });
        return buildingsDOM;
    }

    buildDepartmentResults() {
        let departmentsDOM = this.filterDepartments().map((dept) => {
            return (
                <div key={dept.dept}
                    className="department">
                    {dept.dept}
                </div>
            );
        });
        return departmentsDOM;
    }

    clearSearch() {
        this.props.updateSearchFilter({target: {value: ''}});
    }

    render() {
        let buildings;
        let departments;
        if (this.props.filterText.length > 0) {
            buildings = this.buildBuildingResults();
            departments = this.buildDepartmentResults();
        }
        return (
            <div id="search" className={this.props.visibleComponent === 'search' ? '' : 'hidden'}>
                <BackButtonComponent key="back-search" onPress={this.clearSearch}/>
                
                <div className="search-category">
                    <div className="header">Buildings</div>
                    <div className="results building-results">
                        {buildings}
                    </div>
                </div>

                <div className="search-category">
                    <div className="header">Departments</div>
                    <div className="results departments-results">
                        {departments}
                    </div>
                </div>
            </div>
        );
    }
}

export default SearchComponent;
